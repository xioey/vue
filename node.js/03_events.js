// Event 事件处理模块

const MyEmitter = require("events");

const myEmitter = new MyEmitter();

// 事件绑定监听器处理函数
myEmitter.on("event_name",function(a,b){
    console.log(a,b,this,this === myEmitter);
});

// 事件发射器
myEmitter.emit("event_name","a","b");

// 箭头函数的影响
const myEmitter2 = new MyEmitter();
myEmitter2.on("event_name",(a,b)=>{
    console.log(a,b,this);
});

myEmitter2.emit("event_name","a","b");