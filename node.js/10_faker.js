
const fs = require("fs");
const { times } = require("lodash");
const {faker} = require("@faker-js/faker");
const lipsum = require('lipsum-zh');

faker.locale ="zh_CN";

let users = [];
times(10,index=>{
    users.push({
        id:index+1,
        uuid:faker.datatype.uuid,
        username: `user${faker.datatype.number({min:100,max:999})}`,
        password: faker.internet.password(),
        nickname:faker.lorem.word(),
        truename:faker.name.lastName() + faker.name.firstName(),
        email:faker.helpers.unique(faker.internet.email,[
            faker.lorem.word(3),
            faker.lorem.word(1),
        ]),
        avatar: faker.image.dataUri({width:128,height:128,color:"blue"}),
        role:faker.datatype.number({max:3}),
        login_count: 0,
        last_login_time: faker.datatype.datetime({min:1681014934,max:1682656533000}).valueOf(),
        last_login_ip:faker.internet.ipv4(),
        created_time: faker.datatype.datetime({min:1681014934,max:1682656533000}).valueOf(),
        created_ip:faker.internet.ipv4(),
        updated_time: faker.datatype.datetime({min:1681014934,max:1682656533000}).valueOf(),
    });
});

let posts = [];
times(100,index=>{
    posts.push({
        id:index + 1,
        title:lipsum.generateText(22),
        content: lipsum.generateArticle(200).join("\n"),
        pic:faker.image.imageUrl(),
        read_count:0,
        created_time: faker.datatype.datetime({min:1681014934,max:1682656533000}).valueOf(),
        updated_time: faker.datatype.datetime({min:1681014934,max:1682656533000}).valueOf(),
        user_id:faker.datatype.number({min: 1,max: 10}),
        is_deleted: 0
    });
});

let db ={users: users,posts:posts};
let dbJson =JSON.stringify(db,null,4);

fs.writeFileSync("10_db.json",dbJson);