// 引入包
const http = require("https");
const fs = require("fs");
// 保存图片
const imageUrl =
  "https://tse3-mm.cn.bing.net/th/id/OIP-C.RLXQLqOuMFuWxC1F-zijHAHaGl?pid=ImgDet&rs=1";
const imagePath = "image.jpg";
http.get(imageUrl, (res) => {
  let imageData = "";
  res.setEncoding("binary");
  res.on("data", (chunk) => {
    imageData += chunk;
  });
  res.on("end", () => {
    fs.writeFile(imagePath, imageData, "binary", (err) => {
      if (err) throw err;
      console.log("图片保存成功");
    });
  });
});
