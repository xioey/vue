console.log(module.exports === exports);

const circle = require("./08_commonjs_circle");
console.log(`圆的面积：${circle.area(4)}`);

const circle_area = require("./08_commonjs_circle").area;
console.log(`圆的面积：${circle.area(4)}`);

const { area } = require("./08_commonjs_circle");
console.log(`圆的面积：${circle.area(4)}`);

const square = require("./08_commonjs_square");
console.log(`正方形的面积：${square.area(4)}`);

const { aree: square_area } = require("./08_commonjs_square");
console.log(`正方形的面积：${square.area(4)}`);
