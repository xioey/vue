const arr = Array.from({ length: 100 }, () => Math.floor(Math.random() * 100));
const fs = require("fs");

fs.appendFile("paixu.txt", "排序前的数组为:\n", (error) => {
  if (error) return console.log("写入文件失败,原因是" + error.message);
  console.log("写入成功");
});

fs.appendFile("paixu.txt", arr.join(", ") + "\n", (err) => {
  if (err) throw err;
  console.log('The "data to append" was appended to file!');
});
//插入排序
function sort(arr) {
  for (let i = 1; i < arr.length; i++) {
    let j = i;
    while (j > 0 && arr[j] < arr[j - 1]) {
      [arr[j], arr[j - 1]] = [arr[j - 1], arr[j]];
      j--;
    }
  }
  return arr;
}
// 排序
const sortedArr = sort(arr);
// 以文本字符串的形式写入文本文件

fs.appendFile("paixu.txt", "排序后的数组为:\n", (error) => {
  if (error) return console.log("写入文件失败,原因是" + error.message);
  console.log("写入成功");
});

fs.appendFile("paixu.txt", sortedArr.join(", ") + "\n", (err) => {
  if (err) throw err;
  console.log('The "data to append" was appended to file!');
});
