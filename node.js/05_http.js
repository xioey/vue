// HTTP /HTTPS 模块

const http = require("http");
const https = require("https");

const server = http.createServer(function(request,response){
    console.log(`${require.method} : ${request.url}`);
    response.writeHead(200,{"Content-Type" : "text/html"});
    response.end("<h1>Hello world!</h1>")
});

server.listen(8000);
console.log("服务器运行在http://127.0.0.1:8000");

const request = http.request("http://www.baidu.com",(res)=>{
    console.log(res.statusCode);
});
request.end();

// https访问
https.get({hostname: "www.baidu.com", protocol: "https:" , port: 443 ,method:"GET"},(res)=>{
    console.log(res.headers);
});