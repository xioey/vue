const {PI} = Math;

exports.area = r => PI * 2 ** r;