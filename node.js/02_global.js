// 文件信息相关全局变量
console.log(__dirname);
console.log(__filename);

// console对
console.error("错误信息控制台输出");

// process进程
console.log(process.env.PATH);
console.log(process.pid);
console.log(process.argv);

// URL对象相关
const myURL = new URL("/foo","https://example.com");
myURL.searchParams.append("key","value");
console.log(myURL.href);

let params =new URLSearchParams(myURL.searchParams);
params.append("key1","value1");
params.append("key2","李四");
console.log(params);
myURL.search = params;
console.log(myURL.href);

const buf =Buffer.from("r中国","utf-8");
const strBase64 = buf.toString("base64");
console.log(strBase64);
console.log(Buffer.from(strBase64,"base64").toString("utf-8"));

// 定时器
let timer =setInterval(()=>{
    console.log(`${new Date().toLocaleString()}`);
},1000);

// Ctrl+C终止程序运行时事件处理 处理信号kill
process.on("SIGINT",()=>{
    console.log("系统退出...");
    clearInterval(timer);
});

// process.exit(0);
