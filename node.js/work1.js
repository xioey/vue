// 引入readline模块
const readline = require('readline');
// 控制台获取数据
const rl = readline.createInterface({  input: process.stdin, output: process.stdout });
// question方法，读取
rl.question('请输入表达式:', (exp) => {  
  try {    
  const result = eval(exp);    
  console.log(`表达式的计算结果为：${result}`);  
  } 
  catch (error) {   
     console.log('表达式错误：', error.message); 
     } 
     rl.close();
  });