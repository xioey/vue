//Stream 流模块

const fs = require("fs");

//读取文件流
const rs =fs.createReadStream("06_sample.txt","utf-8");

rs.on("data",function(chunk){
    console.log(`DATA:>>>${chunk}<<<\n`);
});

rs.on("end", function(){
    console.log("文件流读取完成...");
});

rs.on("error", function(err){
    console.log(`ERROR: ${err}`);
});

//文件写入流
const ws1 = fs.createWriteStream("06_output1.txt","utf-8");
ws1.write("使用Stream写入文本数据...\n");
ws1.write(">>>END.");
ws1.end();

const ws2 = fs.createWriteStream("06_output2.txt","utf-8");
ws2.write(Buffer.from("使用Stream写入二进制数据...\n","utf-8"));
ws2.write(Buffer.from(">>>END.","utf-8"));
ws2.end();

//文件流管道：拷贝
const rsSrc = fs.createReadStream("06_sample.txt");
const wsDst = fs.createWriteStream("06_copied.txt");
rsSrc.pipe(wsDst);