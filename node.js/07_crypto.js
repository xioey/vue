// Crypto 加密模块 openssl

const crypto = require("crypto");

const md5 = crypto.createHash("md5");

md5.update("Hello Node.js");
console.log(md5.copy().digest("hex"));

md5.update("111111");
console.log(md5.copy().digest("hex"));

const md5New = crypto.createHmac("md5","secret key");
md5New.update("111111");
console.log(md5New.digest("hex"));

const sha256 = crypto.createHash("sha256");
sha256.update("Hello Node.js");
console.log(sha256.digest("hex"));


