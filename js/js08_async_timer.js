/**
 * 模拟耗时的操作，分时间片，W3C UI响应 < 50ms
 * @param {HTMLElement} msg
 * @param {number} curTotal
 * @param {number} curIndex
 */
function done(msg,curTotal,curIndex){
    if(curTotal<=0){
        msg.innerHTML += "操作完成";
        return ;
    }
    let count =Math.min(curTotal,500000);
    setTimeout(function(){
        let start = performance.now();
        let myDate;
        for(let i=0;i<count;i++){
            let date=new Date();
            myDate=date;
        }
        let end = performance.now();
        console.log(`耗时${end-start}ms,当前剩余${curTotal-count}`);
        done(msg,curTotal-count,curIndex+count);
    },0);
    
}