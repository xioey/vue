async function getPost(postId) {
    const url = `https://jsonplaceholder.typicode.com/posts/${postId}`;
    const response = await fetch(url, { method: "GET" });
    return response.json();
  }
  
  async function getPosts() {
    const url = `https://jsonplaceholder.typicode.com/posts`;
    const response = await fetch(url, { method: "GET" });
    return response.json();
  }
  
  async function createPost(post) {
    const url = `https://jsonplaceholder.typicode.com/posts`;
    const body = JSON.stringify(post);
    const init = {
      method: "POST",
      body: body,
      headers: {
        "Content-type": "application/json;charset=utf-8",
      },
    };
    const response = await fetch(url, init);
    return response.json();
  }
  
  async function fetchImage(imgUrl) {
    const init = {
      method: "GET",
      header: {
        "Content-Type": "image/jpeg",
      },
      mode: "cors",
      cache: "default",
    };
    const response = await fetch(imgUrl, init);
    if (!response.ok) {
      throw new Error("图片资源无法获取");
    }
  
    return response.blob();
  }
  