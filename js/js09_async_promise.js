//创建Promise对象
const myPromise = new Promise((resolve, reject) => {
    let seed = Math.ceil(Math.random() * 10);
    if (seed % 2 === 0) {
      resolve({ value: seed, msg: "获取到偶数" });
    } else {
      reject({ value: seed, msg: "未能获取到偶数" });
    }
  });
  