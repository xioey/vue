/**
 * Student 构造函数
 * @param {string} name 
 * @param {number} age 
 */
function Student(name,age){
    this.name=name;
    this._age=age;
}

/**
 * SayHello
 * @returns {string}
 */
Student.prototype.sayHello =function(){
    return `大家好，我是${this.name},今年${this._age}岁`;
};
/**
 * 年龄Setter
 * @param {number} age 
 */
Student.prototype.setAge =function(age){
    if(age<0||age>120){
        throw "学生年龄值错误！";
    }

    this._age=age;
}
/**
 * 年龄Getter
 * @param {number} age 
 * @returns 
 */
Student.prototype.getAge=function(age){
    return this._age;
}
/**
 * 唯一符号值来做限制访问
 */
const symbolAge =Symbol("age");


/**
 * Student2构造函数
 * @param {string} name 
 */
function Student2(name){
    this.name=name;
    this[symbolAge]=undefined;
}

/**
 * SayHello
 * @returns {string}
 */
Student2.prototype.sayHello =function(){
    return `大家好，我是${this.name},今年${this[symbolAge]}岁`;
};
/**
 * 年龄Setter
 * @param {number} age 
 */
Student2.prototype.setAge =function(age){
    if(age<0||age>120){
        throw "学生年龄值错误！";
    }

    this[symbolAge]=age;
}
/**
 * 年龄Getter
 * @param {number} age 
 * @returns 
 */
Student2.prototype.getAge=function(age){
    return this[symbolAge];
}