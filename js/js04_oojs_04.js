/**
 * 调用函数
 * @param {Object} animal 
 * @returns {String}
 */
function move(animal){
    if(animal.move instanceof Function){
        //move是不是一个函数对象
        return animal.move();
    }
}

function Dog(){}
Dog.prototype.move =function(){
    return `狗在用四条腿移动`
};
function Chicken(){}
Chicken.prototype.move=function(){
    return `鸡在用两条腿移动`;
};