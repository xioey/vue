/**
 * Person1 构造函数
 * @param {string} name 
 */
function Person1(name){
    this.name=name;//属性
    // 行为
    this.introduceSelf=function(){
        return `大家好，我是${name}`;
    };
}
/**
 * Person2 构造函数,定义属性
 * @param {string} name 
 */
function Person2(name){
    this.name=name;
}
// 在原型链上定义要继承的函数
Person2.prototype.introduceSelf =function(){
    return `大家好，我是${this.name}`;
}
// 非原型链方法:属于Person2对象的自身的函数
Person2.mySelf=function(){
    console.log("非原型链上的函数");
}