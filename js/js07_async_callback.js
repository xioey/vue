// 1.同步版分布加法
function doStep1(init){
    return init + 1;
}
function doStep2(init){
    return init + 2;
}
function doStep3(init){
    return init + 3;
}
function doOperation(){
    let result = 0;
    result = doStep1(result);
    result = doStep2(result);
    result = doStep3(result);


    return `同步版结果:${result}`;
}

// 2.异步版
function asyncDoStep1(init,callback){
    const result = init + 1;
    callback(result);
}
function asyncDoStep2(init,callback){
    const result = init + 2;
    callback(result);
}
function asyncDoStep3(init,callback){
    const result = init + 3;
    callback(result);
}
//回调:"回调地狱"
function asyncDoOperation(out){
    asyncDoStep1(0, result1 =>{
        asyncDoStep2(result1, result2=>{
            asyncDoStep3(result2, result3=>{
                out.innerHTML += `异步版结果:${result3}`
            })
        })
    });
}