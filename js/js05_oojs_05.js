class Person{
    /**
     * 构造器
     * @param {String} name 
     */
    constructor(name){
        this.name=name;
    }
    /**
     * Get指示器
     */
    get age(){
        console.log("Age Getter");
        return this._age;
    }
    /**
     * Set指示器
     */
    set age(value){
        console.log(`Age Setter:value - ${value}`);
        this._age=value;
    }
    /**
     * 原型方法
     * @returns {String}
     */
    sayHello(){
        return `大家好，我是${this.name},今年${this.age}`;
    }
    /**
     * 原型方法
     * @returns {String}
     */
    walk(){
        return `行走`;
    }
    /**
     * 静态方法
     * @returns {String}
     */
    static getInstance(){
        return `返回实例`;
    }

}

class Teacher extends Person{
    constructor(name,age,teaches){
        super(name);
        this.age=age;
        this.teaches =teaches;
    }

    set teaches(value){
        this._teaches = value;
    }

    get teaches(){
        return this._teaches;
    }

    sayHello(){
        console.log(super.sayHello());
        return `大家好,我是${this.name},今年${this.age},教《${this.teaches}》课程`;
    }
}

// 类表达式
let Student = class{};