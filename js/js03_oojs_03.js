/**
 * JavaScript中需要继承的都定义在原型链上
 */
/**
 * Person构造函数
 * @param {String} name 
 */
function Person(name){
    this.name =name;
}
//继承的属性
Person.prototype.attr=1;
/**
 * sayHello函数
 * @returns {String}
 */
Person.prototype.sayHello=function(){
    return `大家好，我是${this.name}`;
};
/**
 * Walk函数
 * @returns {String}
 */
Person.prototype.walk=function(){
    return `动起来！！！`;
};
/**
 * Teacher 构造函数
 * @param {String} name 
 * @param {String} teaches 
 */
function Teacher(name,teaches){
    Person.call(this,name);//原型链上有对象的构造函数
    this.teaches =teaches;
}

//组合继承
//1.确保Teacher原型链继承Person的原型链
Teacher.prototype = Object.create(Person.prototype);
//2.确保Teacher构造器的正确指向Teacher构造函数
Teacher.prototype.constructor =Teacher;

Teacher.prototype.sayHello = function(){
    return `大家好，我是${this.name},教《${this.teaches}》课程`;
}
