// async在函数定义时使用，返回值为Promise对象
async function hello1() {
    return "Hello1";
  }
  
  //hello1相当于
  function hello2() {
    return Promise.resolve("Hello2");
  }
  
  //async & await完成 异步代码->同步编写
  function handle1() {
    console.log("第1个处理函数");
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve("第1个处理");
        console.log("第1个处理完成");
      }, 2000);
    });
  }
  
  function handle2() {
    console.log("第2个处理函数");
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve("第2个处理");
        console.log("第2个处理完成");
      }, 1000);
    });
  }
  
  async function handleStart() {
    console.log("使用await关键字");
  
    let str = "";
    let start = performance.now();
    str += await handle1();
    str += "|";
    str += await handle2();
    let end = performance.now();
  
    return { msg: str, time: (end - start) / 1000 };
  }
  