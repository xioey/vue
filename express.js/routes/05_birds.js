/**
 * Bird的路由模块
 */

const express = require("express");
const router = express.Router();

//路由中间件
router.use(function (req, res, next) {
  console.log(`当前时间戳:${new Date().toLocaleString()}`);
  next();
});

//定义路由
router.get("/", function (req, res) {
  res.send("Bird首页");
});

router.get("/about", function (req, res) {
  res.send("Birds关于页");
});
//导出
module.exports = router;
