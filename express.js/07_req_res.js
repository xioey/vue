/**
 * Express.js基本示例
 */
// 导入Express模块
const express = require("express");
// 创建Express应用
const app = express();
// Express http服务端口
const port = 3001;

//使用内置中间件
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

//路由参数
app.get("/:req_id", function (req, res) {
  console.log(req.params);
  console.log(req.query);

  let str = `路由参数(params):${req.params.req_id}\n`;
  str += `请求参数(query):${req.query.key1}\n`;

  res.send(str);
});

//请求数据
app.post("/", function (req, res) {
  console.log(req.body);
  console.log(req.route);

  let str = `请求数据(body):${req.body.msg}\n`;
  str += `${req.ip} - ${req.url} - ${req.route.path}`;

  res.send(str);
});

// 启动Express服务
app.listen(port, function () {
  console.log(`MyApp访问地址:http://127.0.0.1:${port}`);
});
