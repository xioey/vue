/**
 * Express.js Cookie基本示例
 */
// 导入Express模块
const express = require("express");
//导入cookie模块
const cookieParser = require("cookie-parser");
// 创建Express应用
const app = express();
// Express http服务端口
const port = 3001;

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(cookieParser());

// http访问路径
app.get("/", function (req, res) {
  if (req.cookies.remeber) {
    res.send(`记住了,<a href="/forget">忘记</a>!`);
  } else {
    let str = `
        <form method="post">
            <label>
                <input type ="checkbox" name="remeber">记住
            </label>
            <input type="submit" value="提交">
        </form>
    `;
    res.send(str);
  }
});

app.get("/forget", function (req, res) {
  res.clearCookie("remeber");
  res.redirect("back");
});

app.post("/", function (req, res) {
  let minute = 6000;
  if (req.body.remeber) res.cookie("remeber", 1, { maxAge: minute });
  res.redirect("back");
});

// 启动Express服务
app.listen(port, function () {
  console.log(`MyApp访问地址:http://127.0.0.1:${port}`);
});
