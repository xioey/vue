/**
 * Express.js基本示例
 */
// 导入Express模块
const express = require("express");
// 创建Express应用
const app = express();
// Express http服务端口
const port = 3001;

// 路由参数：使用":"开始作为占位符
app.get("/users/:userId/books/:bookId", function (req, res) {
  console.log(req.params);
  res.send(`用户ID${req.params.userId},书籍ID${req.params.bookId}`);
});

//路由处理方式
app.get("/example/a", function (req, res) {
  res.send("处理路由/example/a");
});

app.get(
  "/example/b",
  function (req, res, next) {
    console.log(`处理/example/b的第一个回调函数...`);
    next(); //进入下一个处理
  },
  function (req, res) {
    console.log(`处理/example/b的第二个回调函数...`);
    res.send("处理路由/example/b");
  }
);

const cb0 = function (req, res, next) {
  console.log(`处理/example/c的第一个回调函数...`);
  next();
};

const cb1 = function (req, res, next) {
  console.log(`处理/example/c的第二个回调函数...`);
  next();
};

const cb2 = function (req, res, next) {
  console.log(`处理/example/c的第三个回调函数...`);
  res.send("处理路由/example/c");
};

app.get("/example/c", [cb0, cb1, cb2]);

app.get(
  "/example/d",
  [cb0, cb1],
  function (req, res, next) {
    console.log(`处理/example/d的回调函数`);
    next();
  },
  function (req, res) {
    console.log(`处理/example/d的另外一个回调函数`);
    res.send("处理路由/example/d");
  }
);
// 启动Express服务
app.listen(port, function () {
  console.log(`MyApp访问地址:http://127.0.0.1:${port}`);
});
