/**
 * Express.js基本示例
 */
// 导入Express模块
const express = require("express");
// 创建Express应用
const app = express();
// Express http服务端口
const port = 3001;

// app有路由方法简化路由的定义
app
  .route("/book")
  .get(function (req, res) {
    res.send("读取book信息");
  })
  .post(function (req, res) {
    res.send("新增book信息");
  })
  .put(function (req, res) {
    res.send("修改book信息");
  });

// 导入路由模块文件
const birds = require("./routes/05_birds");
//使用路由中间件 /birds/*
app.use("/birds", birds);

// 启动Express服务
app.listen(port, function () {
  console.log(`MyApp访问地址:http://127.0.0.1:${port}`);
});
