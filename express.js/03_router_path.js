/**
 * Express.js路由路径示例
 */
// 导入Express模块
const express = require("express");
// 创建Express应用
const app = express();
// Express http服务端口
const port = 3001;

// 路由访问路径: /
app.get("/", function (req, res) {
  res.send("匹配/路由");
});

// 路由访问路径: 字符串
app.get("/about", function (req, res) {
  res.send("匹配/about路由");
});

// 路由访问路径:字符串模式 ?
app.get("/ab?cd", function (req, res) {
  res.send("匹配/abcd /acd路由,?表示0或1个b");
});

// 路由访问路径:字符串模式 +
app.get("/xy+z", function (req, res) {
  res.send("匹配/xyz /xyyz /xyy...yz路由,+表示至少出现一个y");
});

// 路由访问路径:字符串模式 +
app.get("/mn*pq", function (req, res) {
  res.send("匹配/mnpq /mn1pq /mn...pq路由,*表示至少出现任意的字符");
});

// 路由访问路径:字符串模式
app.get("/ab(cd)?e", function (req, res) {
  res.send("匹配/abe /abcde 路由,()表示整体出现");
});

// 路由访问路径:正则模式
app.get(/p/, function (req, res) {
  res.send("匹配/p 开始的路由");
});

// 路由访问路径:正则模式
app.get(/.*fly$/, function (req, res) {
  res.send("匹配/...fly 结尾的路由路径");
});

// 启动Express服务
app.listen(port, function () {
  console.log(`MyApp访问地址:http://127.0.0.1:${port}`);
});
