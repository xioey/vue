/**
 * Express.js 视图渲染(PUG)示例
 */
// 导入Express模块
const express = require("express");
// 创建Express应用
const app = express();
// Express http服务端口
const port = 3001;

//定义视图路径
app.set("views", "./views");
//指定视图渲染引擎
app.set("view engine", "pug");

// http访问路径
app.get("/", function (req, res) {
  res.render("10_index", {
    title: "Express支持PUG模板引擎",
    message: "Hello Express.js &Pug",
    dataList: ["数据项1", "数据项2", "数据项3"],
  });
});

// 启动Express服务
app.listen(port, function () {
  console.log(`MyApp访问地址:http://127.0.0.1:${port}`);
});
