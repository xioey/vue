/**
 * 访问test数据库
 */

const mysql = require("mysql");
var pool = mysql.createPool({
  connectionLimit: 10,
  host: "127.0.0.1",
  user: "test",
  password: "123456",
  database: "test",
});

/**
 * 获取SQL执行结果
 * @returns {Promise}
 */
function query(sql, values) {
  return new Promise((resolve, reject) => {
    //连接池中获取连接
    pool.getConnection((error, connection) => {
      if (error) {
        reject(error);
      } else {
        //执行SQL语句
        connection.query(sql, values, (error, results) => {
          //释放连接
          connection.release();
          if (error) {
            reject(error);
          } else {
            resolve(results);
          }
        });
      }
    });
  });
}

module.exports = { query };
