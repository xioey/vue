/**
 * Express.js基本示例
 */
// 导入Express模块
const express = require("express");
// 创建Express应用
const app = express();
// Express http服务端口
const port = 3000;

// http访问路径
app.get("/", function (req, res) {
  res.send("<h1>使用Express.js框架</h1>");
});

// 启动Express服务
app.listen(port, function () {
  console.log(`MyApp访问地址:http://127.0.0.1:${port}`);
});
