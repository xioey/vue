/**
 * Express.js路由方法
 */
// 导入Express模块
const express = require("express");
// 创建Express应用
const app = express();
// Express http服务端口
const port = 3001;

// http动词：get    --获取资源
app.get("/", function (req, res) {
  res.send({ code: 201, msg: "http:get请求" });
});

// http动词:post    --新增资源
app.post("/user", function (req, res) {
  res.json({ code: 201, msg: "http:post请求" });
});

// http动词:put     --更新资源
app.put("/user/1", function (req, res) {
  res.json({ code: 204, msg: "http:put请求" });
});

// http动词：patch   --更新部分资源
app.patch("/user/1", function (req, res) {
  res.json({ code: 204, msg: "http:patch请求" });
});

// http动词:delete     --删除资源
app.delete("/user/1", function (req, res) {
  res.json({ code: 200, msg: "http:delete请求" });
});

// 响应所有请求
app.all("/secret", function (req, res, next) {
  console.log("访问/secret...");
  // next();
  res.json({ code: 200, msg: `All请求,响应http路由方法${req.method}` });
});

// 启动Express服务
app.listen(port, function () {
  console.log(`MyApp访问地址:http://127.0.0.1:${port}`);
});
