/**
 * Express.js Session示例
 */
// 导入Express模块
const express = require("express");
//导入Session模块
const session = require("express-session");
// 创建Express应用
const app = express();
// Express http服务端口
const port = 3001;

//使用session中间件
app.use(
  session({
    secret: "secret key",
    resave: false,
    saveUninitialized: false,
  })
);

// http访问路径
app.get("/", function (req, res) {
  let body = "";
  if (req.session.views) {
    req.session.views += 1;
  } else {
    req.session.views = 1;
    body += `<p>第一次访问</p>`;
  }

  res.send(`${body} <p>已访问<strong>${req.session.views}</strong>次</p>`);
});

// 启动Express服务
app.listen(port, function () {
  console.log(`MyApp访问地址:http://127.0.0.1:${port}`);
});
