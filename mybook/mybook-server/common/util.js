/**
 * 公共功能模块
 */
 
//导入debug模块，并定义两个名字空间
const debug = require("debug")("mybook:[debug]");
const error = require("debug")("mybook:[error]");

//导入加密模块
const crypto = require("crypto"); 

const config = require("./config");
 
 
/**
 * JSONData数据类型说明
 * 
 * @typedef {Object} JSONData
 * @property {Number} code
 * @property {String} msg
 * @property {(Object | Array)} data
 * 
 */
 
/**
 * 统一格式返回的JSON数据
 * 
 * 200 - 访问成功
 * 201 - 新增资源
 * 204 - 删除资源
 * 400 - 错误请求
 * 401 - 无授权访问
 * 
 * @param {Number} code - 返回自定义的状态码
 * @param {String} msg - 提示消息
 * @param {Object | Array} data - 数据结果
 * @returns {JSONData}
 */
function FormatJSONData(code, msg = "", data = []) {
 if(!data){
 data = [];
 }
 
 return {code: code, msg: msg, data: data};
}
 
/**
 * 调试信息
 * @param {String} msg - 调试信息
 */
function log(msg) {
 debug(msg);
}
 
/**
 * 带格式调试信息
 * @param {String} formatter - 带格式占位符的字符串例如 %O,%j
 * @param {Object} data - 数据对象
 */
function logFormat(formatter, data) {
 debug(formatter, data);
}
 
/**
 * 错误信息
 * @param {String} msg - 错误信息
 */
function err(msg) {
  error(msg);
}

/**
 * 生成MD5后的密码
 * @param {String} password 密码
 * @returns {String} 16进制密码串
 */
function md5pwd(password){
  let salt = config.salt;
  let md5 = crypto.createHash("md5");
  return md5.update(password+salt).digest("hex");
}

/**
 *
 * @param {Express.Request} req 请求
 * @returns
 */
function getReqRemoteIP(req) {
  return (req.headers["x-forwarded-ip"] || "").split(",")[0] || req.ip;
}
 
module.exports = {
 FormatJSONData,
 log,
 logFormat,
 err,
 md5pwd,
 getReqRemoteIP,
};