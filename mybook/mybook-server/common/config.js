/**
 * 配置模块
 */

const path = require("path");

module.exports = {
    serverUrl: "http://127.0.0.1:3004",
    dbFile: path.join(__dirname, "../data/mylibrary.db"),//SQLite数据库文件位置
    douban:{
        baseUrl:"https://book.douban.com/isbn/"
    },
    salt: "s$dksksdfs122ddf",

}