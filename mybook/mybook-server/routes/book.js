/**
 * Book路由模块
 */

// /**
//  * @swagger
//  *
//  * components:
//  *   schemas:
//  *     Book:
//  *       required:
//  *         - id
//  *         - title
//  *         - isbn
//  *       type: object
//  *       properties:
//  *         id:
//  *           type: integer
//  *           format: int64
//  *           example: 10
//  *         title:
//  *           type: string
//  *           description: 书名
//  *         pic:
//  *           type: string
//  *           description: 书籍封面远程url
//  *         localPic:
//  *           type: string
//  *           description: 书籍封面本地地址
//  *         producer:
//  *           type: string
//  *           description: 出品方
//  *         subtitle:
//  *           type: string
//  *           description: 副标题
//  *         originalTitle:
//  *           type: string
//  *           description: 原作名
//  *         author:
//  *           type: string
//  *           description: 作者
//  *         publisher:
//  *           type: string
//  *           description: 出版社
//  *         translator:
//  *           type: string
//  *           description: 译者
//  *         pubdate:
//  *           type: string
//  *           description: 出版日期
//  *         pages:
//  *           type: string
//  *           description: 页数
//  *         price:
//  *           type: string
//  *           description: 价格
//  *         binding:
//  *           type: string
//  *           description: 装帧
//  *         series:
//  *           type: string
//  *           description: 系列
//  *         isbn:
//  *           type: string
//  *           description: ISBN号
//  *         intro:
//  *           type: string
//  *           description: 内容简介
//  *         doubanId:
//  *           type: string
//  *           description: 豆瓣读书ID
//  */
var express = require("express");
var router = express.Router();

//导入所需控制器
const { fetchFromDouban } = require("../controller/fetch");
const book = require("../controller/book");
const auth = require("../controller/auth");

//定义路由

//守卫路由：所有路由不想执行权限控制的判断
router.all("*", auth.isLogin);
// TODO:增加新的功能，及增加对应的路由，控制器以及访问数据库的数据模型
// /**
//  * @swagger
//  * /books/fetch/douban/{isbn}:
//  *   get:
//  *     tags:
//  *       - 书籍路由
//  *     summary: 依据ISBN号，从豆瓣获取书籍信息
//  *     description: 返回从豆瓣爬取的书籍详细信息
//  *     parameters:
//  *       - in: path
//  *         name: isbn
//  *         required: true
//  *         schema:
//  *           type: string
//  *         description: 标准isbn号
//  *     responses:
//  *       200:
//  *         description: "获取成功"
//  *         content:
//  *           application/json:
//  *             schema:
//  *               type: object
//  *               properties:
//  *                 code:
//  *                   type: integer
//  *                   format: int32
//  *                 msg:
//  *                   type: string
//  *                 data:
//  *                   $ref: '#/components/schemas/Book'
//  *
//  */
router.get("/fetch/douban/:isbn", fetchFromDouban);
// /**
//  * @swagger
//  * /books/search:
//  *   get:
//  *     tags:
//  *       - 书籍路由
//  *     summary: 依据关键字获取书籍列表信息
//  *     description: 返回符合要求的所有书籍列表信息
//  *     parameters:
//  *       - name: q
//  *         in: query
//  *         description: 查询关键字
//  *         required: true
//  *         explode: true
//  *         schema:
//  *           type: string
//  *       - name: limit
//  *         in: query
//  *         description: 查询数量
//  *         required: false
//  *         explode: true
//  *         schema:
//  *           type: integer
//  *       - name: offset
//  *         in: query
//  *         description: 查询偏移量
//  *         required: false
//  *         explode: true
//  *         schema:
//  *           type: integer
//  *     responses:
//  *       200:
//  *         description: "Web成功访问"
//  *         content:
//  *           application/json:
//  *             schema:
//  *               type: object
//  *               properties:
//  *                 code:
//  *                   type: integer
//  *                   format: int32
//  *                 msg:
//  *                   type: string
//  *                 data:
//  *                   type: array
//  *                   items:
//  *                     $ref: '#/components/schemas/Book'
//  */
router.get("/search", book.search);
// /**
//  * @swagger
//  * /books/isbn/{isbn}:
//  *   get:
//  *     tags:
//  *       - 书籍路由
//  *     summary: 依据ISBN号获取书籍信息
//  *     description: 返回书籍详细信息
//  *     parameters:
//  *       - in: path
//  *         name: isbn
//  *         required: true
//  *         schema:
//  *           type: string
//  *         description: 标准isbn号
//  *     responses:
//  *       200:
//  *         description: "获取成功"
//  *         content:
//  *           application/json:
//  *             schema:
//  *               type: object
//  *               properties:
//  *                 code:
//  *                   type: integer
//  *                   format: int32
//  *                 msg:
//  *                   type: string
//  *                 data:
//  *                   $ref: '#/components/schemas/Book'
//  *
//  */
router.get("/isbn/:isbn", book.findByIsbn);
// /**
//  * @swagger
//  * /books/count:
//  *   get:
//  *     tags:
//  *       - 书籍路由
//  *     summary: 获取书籍总数
//  *     description: 返回书籍总数
//  *     responses:
//  *       200:
//  *         description: "Web成功访问"
//  *         content:
//  *           application/json:
//  *             schema:
//  *               type: object
//  *               properties:
//  *                 code:
//  *                   type: integer
//  *                   format: int32
//  *                 msg:
//  *                   type: string
//  *                 data:
//  *                   type: object
//  *                   properties:
//  *                     total:
//  *                       type: integer
//  */
router.get("/count", book.getCount);
// /**
//  * @swagger
//  * /books/{id}:
//  *   get:
//  *     tags:
//  *       - 书籍路由
//  *     summary: 获取指定ID书籍信息
//  *     description: 返回指定书籍信息
//  *     parameters:
//  *       - in: path
//  *         name: id
//  *         required: true
//  *         schema:
//  *           type: integer
//  *         description: 书籍的内部ID
//  *     responses:
//  *       200:
//  *         description: "Web成功访问"
//  *         content:
//  *           application/json:
//  *             schema:
//  *               type: object
//  *               properties:
//  *                 code:
//  *                   type: integer
//  *                   format: int32
//  *                 msg:
//  *                   type: string
//  *                 data:
//  *                   $ref: '#/components/schemas/Book'
//  */
router.get("/:id", book.find);
// /**
//  * @swagger
//  * /books:
//  *   get:
//  *     tags:
//  *       - 书籍路由
//  *     summary: 获取书籍列表信息
//  *     description: 返回所有书籍列表信息
//  *     parameters:
//  *       - name: limit
//  *         in: query
//  *         description: 查询数量
//  *         required: false
//  *         explode: true
//  *         schema:
//  *           type: integer
//  *       - name: offset
//  *         in: query
//  *         description: 查询偏移量
//  *         required: false
//  *         explode: true
//  *         schema:
//  *           type: integer
//  *     responses:
//  *       200:
//  *         description: "Web成功访问"
//  *         content:
//  *           application/json:
//  *             schema:
//  *               type: object
//  *               properties:
//  *                 code:
//  *                   type: integer
//  *                   format: int32
//  *                 msg:
//  *                   type: string
//  *                 data:
//  *                   type: array
//  *                   items:
//  *                     $ref: '#/components/schemas/Book'
//  */
router.get("/", book.findAll);
// /**
//  * @swagger
//  * /books:
//  *   post:
//  *     tags:
//  *       - 书籍路由
//  *     summary: 新增书籍信息
//  *     description: 新增
//  *     requestBody:
//  *       content:
//  *         application/json:
//  *           schema:
//  *             $ref: "#/components/schemas/Book"
//  *       required: true
//  *     responses:
//  *       200:
//  *         description: "Web成功访问"
//  *         content:
//  *           application/json:
//  *             schema:
//  *               type: object
//  *               properties:
//  *                 code:
//  *                   type: integer
//  *                   format: int32
//  *                 msg:
//  *                   type: string
//  *                 data:
//  *                   type: string
//  *
//  */
router.post("/", book.add);
// /**
//  * @swagger
//  * /books/{id}:
//  *   put:
//  *     tags:
//  *       - 书籍路由
//  *     summary: 修改书籍信息
//  *     description: 修改
//  *     parameters:
//  *       - in: path
//  *         name: id
//  *         required: true
//  *         schema:
//  *           type: integer
//  *         description: 书籍的内部ID
//  *     requestBody:
//  *       content:
//  *         application/json:
//  *           schema:
//  *             $ref: "#/components/schemas/Book"
//  *       required: true
//  *     responses:
//  *       200:
//  *         description: "Web成功访问"
//  *         content:
//  *           application/json:
//  *             schema:
//  *               type: object
//  *               properties:
//  *                 code:
//  *                   type: integer
//  *                   format: int32
//  *                 msg:
//  *                   type: string
//  *                 data:
//  *                   type: string
//  *
//  */
router.put("/:id", book.update);
// /**
//  * @swagger
//  * /books/{id}:
//  *   delete:
//  *     tags:
//  *       - 书籍路由
//  *     summary: 删除书籍信息
//  *     description: 删除
//  *     parameters:
//  *       - in: path
//  *         name: id
//  *         required: true
//  *         schema:
//  *           type: integer
//  *         description: 书籍的内部ID
//  *     responses:
//  *       200:
//  *         description: "Web成功访问"
//  *         content:
//  *           application/json:
//  *             schema:
//  *               type: object
//  *               properties:
//  *                 code:
//  *                   type: integer
//  *                   format: int32
//  *                 msg:
//  *                   type: string
//  *                 data:
//  *                   type: string
//  *
//  */
router.delete("/:id", book.remove);

module.exports = router;
