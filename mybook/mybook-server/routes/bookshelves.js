/**
 * Bookshelf路由模块
 */

// /**
//  * @swagger
//  *
//  * components:
//  *   schemas:
//  *     BookShelf:
//  *       type: object
//  *       properties:
//  *         id:
//  *           type: integer
//  *           format: int64
//  *           example: 10
//  *         book_id:
//  *           type: integer
//  *         user_id:
//  *           type: integer
//  *         read_status:
//  *           type: integer
//  *         ranking:
//  *           type: integer
//  *         created_time:
//  *           type: string
//  *           format: date-time
//  *         updated_time:
//  *           type: string
//  *           format: date-time
//  *         title:
//  *           type: string
//  *           description: 书名
//  *         pic:
//  *           type: string
//  *           description: 书籍封面远程url
//  *         localPic:
//  *           type: string
//  *           description: 书籍封面本地地址
//  *         producer:
//  *           type: string
//  *           description: 出品方
//  *         subtitle:
//  *           type: string
//  *           description: 副标题
//  *         originalTitle:
//  *           type: string
//  *           description: 原作名
//  *         author:
//  *           type: string
//  *           description: 作者
//  *         publisher:
//  *           type: string
//  *           description: 出版社
//  *         translator:
//  *           type: string
//  *           description: 译者
//  *         pubdate:
//  *           type: string
//  *           description: 出版日期
//  *         pages:
//  *           type: string
//  *           description: 页数
//  *         price:
//  *           type: string
//  *           description: 价格
//  *         binding:
//  *           type: string
//  *           description: 装帧
//  *         series:
//  *           type: string
//  *           description: 系列
//  *         isbn:
//  *           type: string
//  *           description: ISBN号
//  *         intro:
//  *           type: string
//  *           description: 内容简介
//  *         doubanId:
//  *           type: string
//  *           description: 豆瓣读书ID
//  */

var express = require("express");
var router = express.Router();

const bookshelf = require("../controller/bookshelf");
const auth = require("../controller/auth");

//定义路由

//守卫路由：所有路由不想执行权限控制的判断
router.all("*", auth.isLogin);

// /**
//  * @swagger
//  * /bookshelves/average-ranking/{book_id}:
//  *   get:
//  *     tags:
//  *       - 书架路由
//  *     summary: 获取指定BookID的书籍综合评分
//  *     description: 返回指定书籍评分信息
//  *     parameters:
//  *       - in: path
//  *         name: book_id
//  *         required: true
//  *         schema:
//  *           type: integer
//  *         description: 书籍ID
//  *     responses:
//  *       200:
//  *         description: "Web成功访问"
//  *         content:
//  *           application/json:
//  *             schema:
//  *               type: object
//  *               properties:
//  *                 code:
//  *                   type: integer
//  *                   format: int32
//  *                 msg:
//  *                   type: string
//  *                 data:
//  *                   $ref: '#/components/schemas/BookShelf'
//  */
router.get("/average-ranking/:book_id", bookshelf.avgRankingByBookId);
// /**
//  * @swagger
//  * /bookshelves/book/{book_id}:
//  *   get:
//  *     tags:
//  *       - 书架路由
//  *     summary: 获取指定BookID的书架书籍信息
//  *     description: 返回指定书架信息
//  *     parameters:
//  *       - in: path
//  *         name: book_id
//  *         required: true
//  *         schema:
//  *           type: integer
//  *         description: 书籍ID
//  *     responses:
//  *       200:
//  *         description: "Web成功访问"
//  *         content:
//  *           application/json:
//  *             schema:
//  *               type: object
//  *               properties:
//  *                 code:
//  *                   type: integer
//  *                   format: int32
//  *                 msg:
//  *                   type: string
//  *                 data:
//  *                   $ref: '#/components/schemas/BookShelf'
//  */

router.get("/book/:book_id", bookshelf.findByBookId);

// /**
//  * @swagger
//  * /bookshelves/read-status/{id}:
//  *   patch:
//  *     tags:
//  *       - 书架路由
//  *     summary: 更新阅读状态
//  *     description: 更新
//  *     parameters:
//  *       - in: path
//  *         name: id
//  *         required: true
//  *         schema:
//  *           type: integer
//  *         description: 书架的内部ID
//  *     requestBody:
//  *       content:
//  *         application/json:
//  *           schema:
//  *             type: object
//  *             properties:
//  *               read_status:
//  *                 type: integer
//  *       required: true
//  *     responses:
//  *       200:
//  *         description: "Web成功访问"
//  *         content:
//  *           application/json:
//  *             schema:
//  *               type: object
//  *               properties:
//  *                 code:
//  *                   type: integer
//  *                   format: int32
//  *                 msg:
//  *                   type: string
//  *                 data:
//  *                   type: string
//  *
//  */

router.patch("/read-status/:id", bookshelf.updateReadStatus);

// /**
//  * @swagger
//  * /bookshelves/ranking/{id}:
//  *   patch:
//  *     tags:
//  *       - 书架路由
//  *     summary: 书架中书籍评分
//  *     description: 评分
//  *     parameters:
//  *       - in: path
//  *         name: id
//  *         required: true
//  *         schema:
//  *           type: integer
//  *         description: 书架的内部ID
//  *     requestBody:
//  *       content:
//  *         application/json:
//  *           schema:
//  *             type: object
//  *             properties:
//  *               ranking:
//  *                 type: integer
//  *       required: true
//  *     responses:
//  *       200:
//  *         description: "Web成功访问"
//  *         content:
//  *           application/json:
//  *             schema:
//  *               type: object
//  *               properties:
//  *                 code:
//  *                   type: integer
//  *                   format: int32
//  *                 msg:
//  *                   type: string
//  *                 data:
//  *                   type: string
//  *
//  */

router.patch("/ranking/:id", bookshelf.updateRanking);

// /**
//  * @swagger
//  * /bookshelves/count:
//  *   get:
//  *     tags:
//  *       - 书架路由
//  *     summary: 获取书架总数
//  *     description: 返回书架总数
//  *     responses:
//  *       200:
//  *         description: "Web成功访问"
//  *         content:
//  *           application/json:
//  *             schema:
//  *               type: object
//  *               properties:
//  *                 code:
//  *                   type: integer
//  *                   format: int32
//  *                 msg:
//  *                   type: string
//  *                 data:
//  *                   type: object
//  *                   properties:
//  *                     total:
//  *                       type: integer
//  */

router.get("/count", bookshelf.getCount);

// /**
//  * @swagger
//  * /bookshelves/{id}:
//  *   get:
//  *     tags:
//  *       - 书架路由
//  *     summary: 获取指定ID书架信息
//  *     description: 返回指定书架信息
//  *     parameters:
//  *       - in: path
//  *         name: id
//  *         required: true
//  *         schema:
//  *           type: integer
//  *         description: 书籍的内部ID
//  *     responses:
//  *       200:
//  *         description: "Web成功访问"
//  *         content:
//  *           application/json:
//  *             schema:
//  *               type: object
//  *               properties:
//  *                 code:
//  *                   type: integer
//  *                   format: int32
//  *                 msg:
//  *                   type: string
//  *                 data:
//  *                   $ref: '#/components/schemas/BookShelf'
//  */

router.get("/:id", bookshelf.find);

// /**
//  * @swagger
//  * /bookshelves:
//  *   get:
//  *     tags:
//  *       - 书架路由
//  *     summary: 获取书架列表信息
//  *     description: 返回所有书架列表信息
//  *     parameters:
//  *       - name: limit
//  *         in: query
//  *         description: 查询数量
//  *         required: false
//  *         explode: true
//  *         schema:
//  *           type: integer
//  *       - name: offset
//  *         in: query
//  *         description: 查询偏移量
//  *         required: false
//  *         explode: true
//  *         schema:
//  *           type: integer
//  *     responses:
//  *       200:
//  *         description: "Web成功访问"
//  *         content:
//  *           application/json:
//  *             schema:
//  *               type: object
//  *               properties:
//  *                 code:
//  *                   type: integer
//  *                   format: int32
//  *                 msg:
//  *                   type: string
//  *                 data:
//  *                   type: array
//  *                   items:
//  *                     $ref: '#/components/schemas/BookShelf'
//  */

router.get("/", bookshelf.findAll);

// /**
//  * @swagger
//  * /bookshelves:
//  *   post:
//  *     tags:
//  *       - 书架路由
//  *     summary: 新增书架信息
//  *     description: 新增
//  *     requestBody:
//  *       content:
//  *         application/json:
//  *           schema:
//  *             type: object
//  *             properties:
//  *               book_id:
//  *                 type: integer
//  *       required: true
//  *     responses:
//  *       200:
//  *         description: "Web成功访问"
//  *         content:
//  *           application/json:
//  *             schema:
//  *               type: object
//  *               properties:
//  *                 code:
//  *                   type: integer
//  *                   format: int32
//  *                 msg:
//  *                   type: string
//  *                 data:
//  *                   type: string
//  *
//  */

router.post("/", bookshelf.add);

// /**
//  * @swagger
//  * /bookshelves/{id}:
//  *   put:
//  *     tags:
//  *       - 书架路由
//  *     summary: 修改书架信息
//  *     description: 修改
//  *     parameters:
//  *       - in: path
//  *         name: id
//  *         required: true
//  *         schema:
//  *           type: integer
//  *         description: 书架的内部ID
//  *     requestBody:
//  *       content:
//  *         application/json:
//  *           schema:
//  *             type: object
//  *             properties:
//  *               read_status:
//  *                 type: integer
//  *               ranking:
//  *                 type: integer
//  *       required: true
//  *     responses:
//  *       200:
//  *         description: "Web成功访问"
//  *         content:
//  *           application/json:
//  *             schema:
//  *               type: object
//  *               properties:
//  *                 code:
//  *                   type: integer
//  *                   format: int32
//  *                 msg:
//  *                   type: string
//  *                 data:
//  *                   type: string
//  *
//  */

router.put("/:id", bookshelf.update);

// /**
//  * @swagger
//  * /bookshelves/{id}:
//  *   delete:
//  *     tags:
//  *       - 书架路由
//  *     summary: 删除书架信息
//  *     description: 删除
//  *     parameters:
//  *       - in: path
//  *         name: id
//  *         required: true
//  *         schema:
//  *           type: integer
//  *         description: 书架记录的内部ID
//  *     responses:
//  *       200:
//  *         description: "Web成功访问"
//  *         content:
//  *           application/json:
//  *             schema:
//  *               type: object
//  *               properties:
//  *                 code:
//  *                   type: integer
//  *                   format: int32
//  *                 msg:
//  *                   type: string
//  *                 data:
//  *                   type: string
//  *
//  */

router.delete("/:id", bookshelf.remove);

module.exports = router;
