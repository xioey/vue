var express = require("express");
var router = express.Router();

const util = require("../common/util");

/**
 * @swagger
 * /:
 *   get:
 *     tags:
 *       - 默认路由
 *     summary: 获取网站的提示信息
 *     description: 默认路由
 *     responses:
 *       200:
 *         description: 默认路由响应
 */
router.get("/", function (req, res, next) {
  let data = {
    siteName: "我的藏书馆",
    siteIcp: "赣ICP备12000882号",
    clientIp: util.getReqRemoteIP(req),
  };
  // res.json({ msg: "默认路由响应" });
  res.json(util.FormatJSONData(200, `系统信息 API Design By Express.js`, data));
});

module.exports = router;
