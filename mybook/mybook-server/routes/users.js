/**
 * User路由模块
 */

var express = require("express");
var router = express.Router();

const user = require("../controller/user");
const auth = require("../controller/auth");

// /**
//  * @swagger
//  * /users/register:
//  *   post:
//  *     tags:
//  *       - 用户路由
//  *     summary: 用户注册
//  *     description: 注册
//  *     requestBody:
//  *       content:
//  *         application/json:
//  *           schema:
//  *             $ref: "#/components/schemas/User"
//  *       required: true
//  *     responses:
//  *       200:
//  *         description: "Web成功访问"
//  *         content:
//  *           application/json:
//  *             schema:
//  *               type: object
//  *               properties:
//  *                 code:
//  *                   type: integer
//  *                   format: int32
//  *                 msg:
//  *                   type: string
//  *                 data:
//  *                   type: string
//  */
router.post("/register", user.register);
// /**
//  * @swagger
//  * /users/login:
//  *   post:
//  *     tags:
//  *       - 用户路由
//  *     summary: 用户登录
//  *     description: 登录
//  *     requestBody:
//  *       content:
//  *         application/json:
//  *           schema:
//  *               type: object
//  *               properties:
//  *                 username:
//  *                   type: string
//  *                   example: user01
//  *                 password:
//  *                   type: string
//  *                   example: 111111
//  *       required: true
//  *     responses:
//  *       200:
//  *         description: "Web成功访问"
//  *         content:
//  *           application/json:
//  *             schema:
//  *               type: object
//  *               properties:
//  *                 code:
//  *                   type: integer
//  *                   format: int32
//  *                 msg:
//  *                   type: string
//  *                 data:
//  *                   type: string
//  *
//  */
router.post("/login", user.login);
// /**
//  * @swagger
//  * /users/logout:
//  *   get:
//  *     tags:
//  *       - 用户路由
//  *     summary: 用户退出
//  *     description: 退出
//  *     responses:
//  *       200:
//  *         description: "Web成功访问"
//  *         content:
//  *           application/json:
//  *             schema:
//  *               type: object
//  *               properties:
//  *                 code:
//  *                   type: integer
//  *                   format: int32
//  *                 msg:
//  *                   type: string
//  *                 data:
//  *                   type: string
//  *
//  */
router.get("/logout", auth.isLogin, user.logout);
// /**
//  * @swagger
//  * /users/check-password:
//  *   patch:
//  *     tags:
//  *       - 用户路由
//  *     summary: 修改用户密码
//  *     description: 返回影响的记录数
//  *     requestBody:
//  *       content:
//  *         application/json:
//  *           schema:
//  *             type: object
//  *             properties:
//  *               password:
//  *                 type: string
//  *       required: true
//  *     responses:
//  *       200:
//  *         description: "Web成功访问"
//  *         content:
//  *           application/json:
//  *             schema:
//  *               type: object
//  *               properties:
//  *                 code:
//  *                   type: integer
//  *                   format: int32
//  *                 msg:
//  *                   type: string
//  *                 data:
//  *                   type: string
//  *
//  */
router.post("/check-password", auth.isLogin, user.checkPassword);
// /**
//  * @swagger
//  * /users/change-info:
//  *   patch:
//  *     tags:
//  *       - 用户路由
//  *     summary: 修改用户个人信息
//  *     description: 返回影响的记录数
//  *     requestBody:
//  *       content:
//  *         application/json:
//  *           schema:
//  *               properties:
//  *                 nickname:
//  *                   type: string
//  *                 truename:
//  *                   type: string
//  *                 avatar:
//  *                   type: string
//  *       required: true
//  *     responses:
//  *       200:
//  *         description: "Web成功访问"
//  *         content:
//  *           application/json:
//  *             schema:
//  *               type: object
//  *               properties:
//  *                 code:
//  *                   type: integer
//  *                   format: int32
//  *                 msg:
//  *                   type: string
//  *                 data:
//  *                   type: string
//  *
//  */
router.patch("/change-info", auth.isLogin, user.changeInfo);
// /**
//  * @swagger
//  * /users/change-password:
//  *   patch:
//  *     tags:
//  *       - 用户路由
//  *     summary: 修改用户密码
//  *     description: 返回影响的记录数
//  *     requestBody:
//  *       content:
//  *         application/json:
//  *           schema:
//  *             type: object
//  *             properties:
//  *               password:
//  *                 type: string
//  *       required: true
//  *     responses:
//  *       200:
//  *         description: "Web成功访问"
//  *         content:
//  *           application/json:
//  *             schema:
//  *               type: object
//  *               properties:
//  *                 code:
//  *                   type: integer
//  *                   format: int32
//  *                 msg:
//  *                   type: string
//  *                 data:
//  *                   type: string
//  *
//  */
router.patch("/change-password", auth.isLogin, user.changePassword);
// /**
//  * @swagger
//  * /users/reset-password/{id}:
//  *   patch:
//  *     tags:
//  *       - 用户路由
//  *     summary: 重置用户密码
//  *     description: 返回影响的记录数
//  *     parameters:
//  *       - in: path
//  *         name: id
//  *         required: true
//  *         schema:
//  *           type: integer
//  *         description: 用户的内部ID
//  *     responses:
//  *       200:
//  *         description: "Web成功访问"
//  *         content:
//  *           application/json:
//  *             schema:
//  *               type: object
//  *               properties:
//  *                 code:
//  *                   type: integer
//  *                   format: int32
//  *                 msg:
//  *                   type: string
//  *                 data:
//  *                   type: string
//  *
//  */
router.patch(
  "/reset-password/:id",
  auth.isLogin,
  auth.isAdmin,
  user.resetPassword
);
// /**
//  * @swagger
//  * /users/count:
//  *   get:
//  *     tags:
//  *       - 用户路由
//  *     summary: 获取用户总数
//  *     description: 返回用户总数
//  *     responses:
//  *       200:
//  *         description: "Web成功访问"
//  *         content:
//  *           application/json:
//  *             schema:
//  *               type: object
//  *               properties:
//  *                 code:
//  *                   type: integer
//  *                   format: int32
//  *                 msg:
//  *                   type: string
//  *                 data:
//  *                   type: object
//  *                   properties:
//  *                     total:
//  *                       type: integer
//  */
router.get("/count", auth.isLogin, auth.isAdmin, user.getCount);
// /**
//  * @swagger
//  * /users/search:
//  *   get:
//  *     tags:
//  *       - 用户路由
//  *     summary: 依据关键字获取用户列表信息
//  *     description: 返回符合要求的所有用户列表信息
//  *     parameters:
//  *       - name: q
//  *         in: query
//  *         description: 查询关键字
//  *         required: true
//  *         explode: true
//  *         schema:
//  *           type: string
//  *       - name: limit
//  *         in: query
//  *         description: 查询数量
//  *         required: false
//  *         explode: true
//  *         schema:
//  *           type: integer
//  *       - name: offset
//  *         in: query
//  *         description: 查询偏移量
//  *         required: false
//  *         explode: true
//  *         schema:
//  *           type: integer
//  *     responses:
//  *       200:
//  *         description: "Web成功访问"
//  *         content:
//  *           application/json:
//  *             schema:
//  *               type: object
//  *               properties:
//  *                 code:
//  *                   type: integer
//  *                   format: int32
//  *                 msg:
//  *                   type: string
//  *                 data:
//  *                   type: array
//  *                   items:
//  *                     $ref: '#/components/schemas/User'
//  */
router.get("/search", auth.isLogin, auth.isAdmin, user.search);
// /**
//  * @swagger
//  * /users/{id}:
//  *   get:
//  *     tags:
//  *       - 用户路由
//  *     summary: 获取指定ID用户信息
//  *     description: 返回指定用户信息
//  *     parameters:
//  *       - in: path
//  *         name: id
//  *         required: true
//  *         schema:
//  *           type: integer
//  *         description: 用户的内部ID
//  *     responses:
//  *       200:
//  *         description: "Web成功访问"
//  *         content:
//  *           application/json:
//  *             schema:
//  *               type: object
//  *               properties:
//  *                 code:
//  *                   type: integer
//  *                   format: int32
//  *                 msg:
//  *                   type: string
//  *                 data:
//  *                   $ref: '#/components/schemas/User'
//  */
router.get("/:id", auth.isLogin,auth.isSelfOfAdmin, user.find);
// /**
//  * @swagger
//  * /users:
//  *   get:
//  *     tags:
//  *       - 用户路由
//  *     summary: 获取用户列表信息
//  *     description: 返回所有用户列表信息
//  *     parameters:
//  *       - name: limit
//  *         in: query
//  *         description: 查询数量
//  *         required: false
//  *         explode: true
//  *         schema:
//  *           type: integer
//  *       - name: offset
//  *         in: query
//  *         description: 查询偏移量
//  *         required: false
//  *         explode: true
//  *         schema:
//  *           type: integer
//  *     responses:
//  *       200:
//  *         description: "Web成功访问"
//  *         content:
//  *           application/json:
//  *             schema:
//  *               type: object
//  *               properties:
//  *                 code:
//  *                   type: integer
//  *                   format: int32
//  *                 msg:
//  *                   type: string
//  *                 data:
//  *                   type: array
//  *                   items:
//  *                     $ref: '#/components/schemas/User'
//  */
router.get("/", auth.isLogin, auth.isAdmin, user.findAll);
// /**
//  * @swagger
//  * /users:
//  *   post:
//  *     tags:
//  *       - 用户路由
//  *     summary: 新增用户信息
//  *     description: 返回新增用户ID
//  *     requestBody:
//  *       content:
//  *         application/json:
//  *           schema:
//  *             $ref: "#/components/schemas/User"
//  *       required: true
//  *     responses:
//  *       200:
//  *         description: "Web成功访问"
//  *         content:
//  *           application/json:
//  *             schema:
//  *               type: object
//  *               properties:
//  *                 code:
//  *                   type: integer
//  *                   format: int32
//  *                 msg:
//  *                   type: string
//  *                 data:
//  *                   type: string
//  *
//  */
router.post("/", auth.isLogin, auth.isAdmin, user.add);
// /**
//  * @swagger
//  * /users/{id}:
//  *   put:
//  *     tags:
//  *       - 用户路由
//  *     summary: 修改用户信息
//  *     description: 返回影响的记录数
//  *     parameters:
//  *       - in: path
//  *         name: id
//  *         required: true
//  *         schema:
//  *           type: integer
//  *         description: 用户的内部ID
//  *     requestBody:
//  *       content:
//  *         application/json:
//  *           schema:
//  *             $ref: "#/components/schemas/User"
//  *       required: true
//  *     responses:
//  *       200:
//  *         description: "Web成功访问"
//  *         content:
//  *           application/json:
//  *             schema:
//  *               type: object
//  *               properties:
//  *                 code:
//  *                   type: integer
//  *                   format: int32
//  *                 msg:
//  *                   type: string
//  *                 data:
//  *                   type: string
//  *
//  */
router.put("/:id", auth.isLogin, auth.isAdmin, user.update);
// /**
//  * @swagger
//  * /users/{id}:
//  *   delete:
//  *     tags:
//  *       - 用户路由
//  *     summary: 删除用户信息
//  *     description: 返回影响的记录数
//  *     parameters:
//  *       - in: path
//  *         name: id
//  *         required: true
//  *         schema:
//  *           type: integer
//  *         description: 用户的内部ID
//  *     responses:
//  *       200:
//  *         description: "Web成功访问"
//  *         content:
//  *           application/json:
//  *             schema:
//  *               type: object
//  *               properties:
//  *                 code:
//  *                   type: integer
//  *                   format: int32
//  *                 msg:
//  *                   type: string
//  *                 data:
//  *                   type: string
//  *
//  */
router.delete("/:id", auth.isLogin, auth.isAdmin, user.remove);

module.exports = router;
