/**
 * Review路由模块
 */

// /**
//  * @swagger
//  *
//  * components:
//  *   schemas:
//  *     BookReview:
//  *       type: object
//  *       properties:
//  *         id:
//  *           type: integer
//  *           format: int64
//  *           example: 10
//  *         book_id:
//  *           type: integer
//  *         user_id:
//  *           type: integer
//  *         title:
//  *           type: string
//  *         content:
//  *           type: string
//  *         useful:
//  *           type: integer
//  *         useless:
//  *           type: integer
//  *         created_time:
//  *           type: string
//  *           format: date-time
//  *         updated_time:
//  *           type: string
//  *           format: date-time
//  *         bookTitle:
//  *           type: string
//  *           description: 书名
//  *         pic:
//  *           type: string
//  *           description: 书籍封面远程url
//  *         localPic:
//  *           type: string
//  *           description: 书籍封面本地地址
//  *         producer:
//  *           type: string
//  *           description: 出品方
//  *         subtitle:
//  *           type: string
//  *           description: 副标题
//  *         originalTitle:
//  *           type: string
//  *           description: 原作名
//  *         author:
//  *           type: string
//  *           description: 作者
//  *         publisher:
//  *           type: string
//  *           description: 出版社
//  *         translator:
//  *           type: string
//  *           description: 译者
//  *         pubdate:
//  *           type: string
//  *           description: 出版日期
//  *         pages:
//  *           type: string
//  *           description: 页数
//  *         price:
//  *           type: string
//  *           description: 价格
//  *         binding:
//  *           type: string
//  *           description: 装帧
//  *         series:
//  *           type: string
//  *           description: 系列
//  *         isbn:
//  *           type: string
//  *           description: ISBN号
//  *         intro:
//  *           type: string
//  *           description: 内容简介
//  *         doubanId:
//  *           type: string
//  *           description: 豆瓣读书ID
//  *         username:
//  *           type: string
//  *         nickname:
//  *           type: string
//  */

// /**
//  * @swagger
//  *
//  * components:
//  *   schemas:
//  *     Review:
//  *       type: object
//  *       properties:
//  *         id:
//  *           type: integer
//  *           format: int64
//  *         book_id:
//  *           type: integer
//  *         user_id:
//  *           type: integer
//  *         title:
//  *           type: string
//  *         content:
//  *           type: string
//  *         useful:
//  *           type: integer
//  *         useless:
//  *           type: integer
//  *         created_time:
//  *           type: string
//  *           format: date-time
//  *         updated_time:
//  *           type: string
//  *           format: date-time
//  */

var express = require("express");
var router = express.Router();

const review = require("../controller/review");
const auth = require("../controller/auth");

//定义路由

//守卫路由：所有路由不想执行权限控制的判断
router.all("*", auth.isLogin);

// /**
//  * @swagger
//  * /reviews/user/count:
//  *   get:
//  *     tags:
//  *       - 评论路由
//  *     summary: 获取指定用户的评论总数
//  *     description: 返回指定用户的评论总数
//  *     parameters:
//  *       - name: user_id
//  *         in: query
//  *         description: 用户id
//  *         required: false
//  *         explode: true
//  *         schema:
//  *           type: integer
//  *     responses:
//  *       200:
//  *         description: "Web成功访问"
//  *         content:
//  *           application/json:
//  *             schema:
//  *               type: object
//  *               properties:
//  *                 code:
//  *                   type: integer
//  *                   format: int32
//  *                 msg:
//  *                   type: string
//  *                 data:
//  *                   type: object
//  *                   properties:
//  *                     total:
//  *                       type: integer
//  */

router.get("/user/count", review.getCountByUserId);

// /**
//  * @swagger
//  * /reviews/user:
//  *   get:
//  *     tags:
//  *       - 评论路由
//  *     summary: 获取评论列表信息
//  *     description: 返回所有评论列表信息
//  *     parameters:
//  *       - name: user_id
//  *         in: query
//  *         description: 用户id
//  *         required: false
//  *         explode: true
//  *         schema:
//  *           type: integer
//  *       - name: order_by
//  *         in: query
//  *         description: 排序字段
//  *         required: false
//  *         explode: true
//  *         schema:
//  *           type: string
//  *       - name: sort
//  *         in: query
//  *         description: 排序方式
//  *         required: false
//  *         explode: true
//  *         schema:
//  *           type: string
//  *       - name: limit
//  *         in: query
//  *         description: 查询数量
//  *         required: false
//  *         explode: true
//  *         schema:
//  *           type: integer
//  *       - name: offset
//  *         in: query
//  *         description: 查询偏移量
//  *         required: false
//  *         explode: true
//  *         schema:
//  *           type: integer
//  *     responses:
//  *       200:
//  *         description: "Web成功访问"
//  *         content:
//  *           application/json:
//  *             schema:
//  *               type: object
//  *               properties:
//  *                 code:
//  *                   type: integer
//  *                   format: int32
//  *                 msg:
//  *                   type: string
//  *                 data:
//  *                   type: array
//  *                   items:
//  *                     $ref: '#/components/schemas/BookReview'
//  */

router.get("/user", review.findAllByUserId);

// /**
//  * @swagger
//  * /reviews/useful/{id}:
//  *   patch:
//  *     tags:
//  *       - 评论路由
//  *     summary: 评论有用
//  *     description: 评论有用
//  *     parameters:
//  *       - in: path
//  *         name: id
//  *         required: true
//  *         schema:
//  *           type: integer
//  *         description: 评论的内部ID
//  *     responses:
//  *       200:
//  *         description: "Web成功访问"
//  *         content:
//  *           application/json:
//  *             schema:
//  *               type: object
//  *               properties:
//  *                 code:
//  *                   type: integer
//  *                   format: int32
//  *                 msg:
//  *                   type: string
//  *                 data:
//  *                   type: string
//  *
//  */

router.patch("/useful/:id", review.updateUseful);

// /**
//  * @swagger
//  * /reviews/useless/{id}:
//  *   patch:
//  *     tags:
//  *       - 评论路由
//  *     summary: 评论无用
//  *     description: 评论无用
//  *     parameters:
//  *       - in: path
//  *         name: id
//  *         required: true
//  *         schema:
//  *           type: integer
//  *         description: 评论的内部ID
//  *     responses:
//  *       200:
//  *         description: "Web成功访问"
//  *         content:
//  *           application/json:
//  *             schema:
//  *               type: object
//  *               properties:
//  *                 code:
//  *                   type: integer
//  *                   format: int32
//  *                 msg:
//  *                   type: string
//  *                 data:
//  *                   type: string
//  *
//  */

router.patch("/useless/:id", review.updateUseless);

// /**
//  * @swagger
//  * /reviews/count:
//  *   get:
//  *     tags:
//  *       - 评论路由
//  *     summary: 获取指定书籍的评论总数
//  *     description: 返回指定书籍的评论总数
//  *     parameters:
//  *       - name: book_id
//  *         in: query
//  *         description: 书籍id
//  *         required: false
//  *         explode: true
//  *         schema:
//  *           type: integer
//  *     responses:
//  *       200:
//  *         description: "Web成功访问"
//  *         content:
//  *           application/json:
//  *             schema:
//  *               type: object
//  *               properties:
//  *                 code:
//  *                   type: integer
//  *                   format: int32
//  *                 msg:
//  *                   type: string
//  *                 data:
//  *                   type: object
//  *                   properties:
//  *                     total:
//  *                       type: integer
//  */

router.get("/count", review.getCount);

// /**
//  * @swagger
//  * /reviews/{id}:
//  *   get:
//  *     tags:
//  *       - 评论路由
//  *     summary: 获取指定ID评论信息
//  *     description: 返回指定评论信息
//  *     parameters:
//  *       - in: path
//  *         name: id
//  *         required: true
//  *         schema:
//  *           type: integer
//  *         description: 评论的内部ID
//  *     responses:
//  *       200:
//  *         description: "Web成功访问"
//  *         content:
//  *           application/json:
//  *             schema:
//  *               type: object
//  *               properties:
//  *                 code:
//  *                   type: integer
//  *                   format: int32
//  *                 msg:
//  *                   type: string
//  *                 data:
//  *                   $ref: '#/components/schemas/BookReview'
//  */

router.get("/:id", review.find);

// /**
//  * @swagger
//  * /reviews:
//  *   get:
//  *     tags:
//  *       - 评论路由
//  *     summary: 获取评论列表信息
//  *     description: 返回所有评论列表信息
//  *     parameters:
//  *       - name: book_id
//  *         in: query
//  *         description: 书籍id
//  *         required: false
//  *         explode: true
//  *         schema:
//  *           type: integer
//  *       - name: order_by
//  *         in: query
//  *         description: 排序字段
//  *         required: false
//  *         explode: true
//  *         schema:
//  *           type: string
//  *       - name: sort
//  *         in: query
//  *         description: 排序方式
//  *         required: false
//  *         explode: true
//  *         schema:
//  *           type: string
//  *       - name: limit
//  *         in: query
//  *         description: 查询数量
//  *         required: false
//  *         explode: true
//  *         schema:
//  *           type: integer
//  *       - name: offset
//  *         in: query
//  *         description: 查询偏移量
//  *         required: false
//  *         explode: true
//  *         schema:
//  *           type: integer
//  *     responses:
//  *       200:
//  *         description: "Web成功访问"
//  *         content:
//  *           application/json:
//  *             schema:
//  *               type: object
//  *               properties:
//  *                 code:
//  *                   type: integer
//  *                   format: int32
//  *                 msg:
//  *                   type: string
//  *                 data:
//  *                   type: array
//  *                   items:
//  *                     $ref: '#/components/schemas/BookReview'
//  */

router.get("/", review.findAll);

// /**
//  * @swagger
//  * /reviews:
//  *   post:
//  *     tags:
//  *       - 评论路由
//  *     summary: 新增评论信息
//  *     description: 新增
//  *     requestBody:
//  *       content:
//  *         application/json:
//  *           schema:
//  *             type: object
//  *             properties:
//  *               $ref: '#/components/schemas/Review'
//  *       required: true
//  *     responses:
//  *       200:
//  *         description: "Web成功访问"
//  *         content:
//  *           application/json:
//  *             schema:
//  *               type: object
//  *               properties:
//  *                 code:
//  *                   type: integer
//  *                   format: int32
//  *                 msg:
//  *                   type: string
//  *                 data:
//  *                   type: string
//  *
//  */

router.post("/", review.add);

// /**
//  * @swagger
//  * /reviews/{id}:
//  *   put:
//  *     tags:
//  *       - 评论路由
//  *     summary: 修改评论信息
//  *     description: 修改
//  *     parameters:
//  *       - in: path
//  *         name: id
//  *         required: true
//  *         schema:
//  *           type: integer
//  *         description: 书架的内部ID
//  *     requestBody:
//  *       content:
//  *         application/json:
//  *           schema:
//  *             type: object
//  *             properties:
//  *               $ref: '#/components/schemas/Review'
//  *       required: true
//  *     responses:
//  *       200:
//  *         description: "Web成功访问"
//  *         content:
//  *           application/json:
//  *             schema:
//  *               type: object
//  *               properties:
//  *                 code:
//  *                   type: integer
//  *                   format: int32
//  *                 msg:
//  *                   type: string
//  *                 data:
//  *                   type: string
//  *
//  */

router.put("/:id", review.update);

// /**
//  * @swagger
//  * /reviews/{id}:
//  *   delete:
//  *     tags:
//  *       - 评论路由
//  *     summary: 删除评论信息
//  *     description: 删除
//  *     parameters:
//  *       - in: path
//  *         name: id
//  *         required: true
//  *         schema:
//  *           type: integer
//  *         description: 书架记录的内部ID
//  *     responses:
//  *       200:
//  *         description: "Web成功访问"
//  *         content:
//  *           application/json:
//  *             schema:
//  *               type: object
//  *               properties:
//  *                 code:
//  *                   type: integer
//  *                   format: int32
//  *                 msg:
//  *                   type: string
//  *                 data:
//  *                   type: string
//  *
//  */

router.delete("/:id", review.remove);

module.exports = router;
