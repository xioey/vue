/**
 * 书籍控制器模块
 * controller\review.js
 *
 * 说明：
 * 中间件处理路由请求：
 * 1. req.params --> 路由参数 /:id  -> req.params.id
 * 2. req.query  --> url查询串  ?k1=v1&k2=v2  -> req.query.k1, req.query.k2
 * 3. req.body   --> 数据对象  {k1:v1, k2:v2} -> req.body.k1, req.body.k2
 *
 * 中间件进行异步封装（async/await），不会直接执行，需要进行函数立即执行的方式。
 */

const express = require("express");

const util = require("../common/util");
/**
 * @typedef {ReviewDB}
 */
const ReviewDB = require("../model/sqlite/review");

/**
 * 获取评论信息
 * @param {express.Request} req  请求
 * @param {express.Response} res  响应
 */
function find(req, res) {
  (async function () {
    let db = ReviewDB.getInstance();
    await db.connect();
    let result = await db.find(req.params.id);
    //await db.close();
    util.logFormat(`获取【${req.params.id}】评论信息%O`, result);
    res.json(util.FormatJSONData(200, `获取评论信息`, result));
  })().catch((err) => {
    util.err(err);
    res.json(util.FormatJSONData(500, "服务器发生意外情况，无法完成请求！"));
  });
}

/**
 * 获取指定书籍的评论信息列表
 * @param {express.Request} req  请求
 * @param {express.Response} res  响应
 */
function findAll(req, res) {
  (async function () {
    //1.处理输入的数据
    //FIXME: 输入数据的正确性校验，一致性校验，完整性校验
    let bookId = req.query.book_id ? req.query.book_id : -1;
    let orderBy = req.query.order_by ? req.query.order_by : "id";
    let sort = req.query.sort ? req.query.sort : "desc";
    let limit = req.query.limit ? req.query.limit : -1;
    let offset = req.query.offset ? req.query.offset : -1;
    //2. 访问数据
    let db = ReviewDB.getInstance();
    await db.connect();
    let result = await db.findAll(bookId, orderBy, sort, limit, offset);
    //await db.close();

    //3.输出数据
    util.logFormat(`获取指定用户【${bookId}的书籍评论信息列表%O`, result);
    res.json(util.FormatJSONData(200, `获取指定书籍评论信息列表`, result));
  })().catch((err) => {
    util.err(err);
    res.json(util.FormatJSONData(500, "服务器发生意外情况，无法完成请求！"));
  });
}

/**
 * 新增评论信息
 * @param {express.Request} req  请求
 * @param {express.Response} res  响应
 */
function add(req, res) {
  (async function () {
    //FIXME:req.body的数据安全性校验
    let review = {
      userId: req.session.user.id,
      bookId: req.body.bookId,
      title: req.body.title,
      content: req.body.content,
      createdTime: Date.now(),
      updatedTime: Date.now(),
    };
    let db = ReviewDB.getInstance();
    await db.connect();
    let result = await db.add(review);
    //await db.close();
    util.log(`新增指定书籍的评论信息lastID->${result}`);
    res.json(util.FormatJSONData(201, `新增书籍评论信息`, { lastID: result }));
  })().catch((err) => {
    util.err(err);
    res.json(util.FormatJSONData(500, "服务器发生意外情况，无法完成请求！"));
  });
}

/**
 * 更新评论信息
 * @param {express.Request} req  请求
 * @param {express.Response} res  响应
 */
function update(req, res) {
  (async function () {
    //FIXME:req.body的数据安全性校验
    let review = {
      id: req.params.id,
      title: req.body.title,
      content: req.body.content,
      updatedTime: Date.now(),
    };

    let db = ReviewDB.getInstance();
    await db.connect();
    let result = await db.update(review);
    //await db.close();
    util.log(`更新指定评论信息：changes->${result}`);
    res.json(util.FormatJSONData(200, `更新指定评论信息`, { changes: result }));
  })().catch((err) => {
    util.err(err);
    res.json(util.FormatJSONData(500, "服务器发生意外情况，无法完成请求！"));
  });
}

/**
 * 删除评论信息
 * @param {express.Request} req  请求
 * @param {express.Response} res  响应
 */
function remove(req, res) {
  (async function () {
    //FIXME:数据合法性校验
    let reviewId = req.params.id;
    let db = ReviewDB.getInstance();
    await db.connect();
    let result = await db.remove(reviewId);
    //await db.close();
    util.log(`删除评论信息：changes->${result}`);
    res.json(util.FormatJSONData(204, `删除评论信息`, { changes: result }));
  })().catch((err) => {
    util.err(err);
    res.json(util.FormatJSONData(500, "服务器发生意外情况，无法完成请求！"));
  });
}

/**
 * 获取指定书籍的评论总数
 * @param {express.Request} req  请求
 * @param {express.Response} res  响应
 */
function getCount(req, res) {
  (async function () {
    //FIXME:数据合法性校验
    let bookId = req.query.book_id;
    let db = ReviewDB.getInstance();
    await db.connect();
    let result = await db.getCount(bookId);
    //await db.close();
    util.log(`指定书籍【${bookId}】评论总数：%O`, result);
    res.json(util.FormatJSONData(200, `指定书籍评论总数`, result));
  })().catch((err) => {
    util.err(err);
    res.json(util.FormatJSONData(500, "服务器发生意外情况，无法完成请求！"));
  });
}

/**
 * 更新评论有用状态
 * @param {express.Request} req  请求
 * @param {express.Response} res  响应
 */
function updateUseful(req, res) {
  (async function () {
    //FIXME:req.body的数据安全性校验
    let id = req.params.id;

    let db = ReviewDB.getInstance();
    await db.connect();
    let result = await db.updateUseful(id);
    //await db.close();
    util.log(`更新指定评论有用：changes->${result}`);
    res.json(util.FormatJSONData(200, `更新指定评论有用`, { changes: result }));
  })().catch((err) => {
    util.err(err);
    res.json(util.FormatJSONData(500, "服务器发生意外情况，无法完成请求！"));
  });
}

/**
 * 更新评论无用状态
 * @param {express.Request} req  请求
 * @param {express.Response} res  响应
 */
function updateUseless(req, res) {
  (async function () {
    //FIXME:req.body的数据安全性校验
    let id = req.params.id;

    let db = ReviewDB.getInstance();
    await db.connect();
    let result = await db.updateUseless(id);
    //await db.close();
    util.log(`更新指定评论无用：changes->${result}`);
    res.json(util.FormatJSONData(200, `更新指定评论无用`, { changes: result }));
  })().catch((err) => {
    util.err(err);
    res.json(util.FormatJSONData(500, "服务器发生意外情况，无法完成请求！"));
  });
}

/**
 * 获取指定用户的评论信息列表
 * @param {express.Request} req  请求
 * @param {express.Response} res  响应
 */
function findAllByUserId(req, res) {
  (async function () {
    //1.处理输入的数据
    //FIXME: 输入数据的正确性校验，一致性校验，完整性校验
    let userId = req.query.user_id;
    let orderBy = req.query.order_by ? req.query.order_by : "id";
    let sort = req.query.sort ? req.query.sort : "desc";
    let limit = req.query.limit ? req.query.limit : -1;
    let offset = req.query.offset ? req.query.offset : -1;
    //2. 访问数据
    let db = ReviewDB.getInstance();
    await db.connect();
    let result = await db.findAllByUserId(userId, orderBy, sort, limit, offset);
    //await db.close();

    //3.输出数据
    util.logFormat(`获取指定用户【${userId}的评论信息列表%O`, result);
    res.json(util.FormatJSONData(200, `获取指定用户评论信息列表`, result));
  })().catch((err) => {
    util.err(err);
    res.json(util.FormatJSONData(500, "服务器发生意外情况，无法完成请求！"));
  });
}

/**
 * 获取指定书籍的评论总数
 * @param {express.Request} req  请求
 * @param {express.Response} res  响应
 */
function getCountByUserId(req, res) {
  (async function () {
    //FIXME:数据合法性校验
    let userId = req.query.user_id;
    let db = ReviewDB.getInstance();
    await db.connect();
    let result = await db.getCountByUserId(userId);
    //await db.close();
    util.log(`指定用户【${userId}】评论总数：%O`, result);
    res.json(util.FormatJSONData(200, `指定用户评论总数`, result));
  })().catch((err) => {
    util.err(err);
    res.json(util.FormatJSONData(500, "服务器发生意外情况，无法完成请求！"));
  });
}

module.exports = {
  find,
  findAll,
  add,
  update,
  remove,
  getCount,
  updateUseful,
  updateUseless,
  findAllByUserId,
  getCountByUserId,
};
