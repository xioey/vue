/**
 * 数据库初始化中间件模块
 */

const SqliteDB = require("../model/sqlite/init");
const util = require("../common/util");

module.exports = function (){
    new SqliteDB();
    return function(req, res, next){
        util.log("执行中间件：数据库初始化！");
        next();
    }
}