/**
 * 书架控制器模块
 * controller\bookshelf.js
 *
 * 说明：
 * 中间件处理路由请求：
 * 1. req.params --> 路由参数 /:id  -> req.params.id
 * 2. req.query  --> url查询串  ?k1=v1&k2=v2  -> req.query.k1, req.query.k2
 * 3. req.body   --> 数据对象  {k1:v1, k2:v2} -> req.body.k1, req.body.k2
 *
 * 中间件进行异步封装（async/await），不会直接执行，需要进行函数立即执行的方式。
 */

const express = require("express");

const util = require("../common/util");
/**
 * @typedef {BookshelfDB}
 */
const BookshelfDB = require("../model/sqlite/bookshelf");

/**
 * 获取书架信息
 * @param {express.Request} req  请求
 * @param {express.Response} res  响应
 */
function find(req, res) {
  (async function () {
    let db = BookshelfDB.getInstance();
    await db.connect();
    let result = await db.find(req.params.id, req.session.user.id);
    //await db.close();
    util.logFormat(`获取【${req.params.id}】书籍信息%O`, result);
    res.json(
      util.FormatJSONData(200, `获取书籍信息【${req.params.id}】`, result)
    );
  })();
}

/**
 * 获取书籍信息列表
 * @param {express.Request} req  请求
 * @param {express.Response} res  响应
 */
function findAll(req, res) {
  (async function () {
    //1.处理输入的数据
    //FIXME: 输入数据的正确性校验，一致性校验，完整性校验
    let userId = req.session.user.id;
    let orderBy = req.query.order_by ? req.query.order_by : "id";
    let sort = req.query.sort ? req.query.sort : "desc";
    let limit = req.query.limit ? req.query.limit : -1;
    let offset = req.query.offset ? req.query.offset : -1;
    //2. 访问数据
    let db = BookshelfDB.getInstance();
    await db.connect();
    let result = await db.findAll(userId, orderBy, sort, limit, offset);
    //await db.close();

    //3.输出数据
    util.logFormat(`获取指定用户【${userId}的书架书籍信息%O`, result);
    res.json(util.FormatJSONData(200, `获取用户书架书籍信息列表`, result));
  })();
}

/**
 * 新增书籍信息
 * @param {express.Request} req  请求
 * @param {express.Response} res  响应
 */
function add(req, res) {
  (async function () {
    //FIXME:req.body的数据安全性校验
    let bookshelf = {
      userId: req.session.user.id,
      bookId: req.body.bookId,
      createdTime: Date.now(),
      updatedTime: Date.now(),
    };
    let db = BookshelfDB.getInstance();
    await db.connect();
    let result = await db.add(bookshelf);
    //await db.close();
    util.log(`新增书架书籍信息lastID->${result}`);
    res.json(
      util.FormatJSONData(201, `书架中添加书籍成功`, { lastID: result })
    );
  })();
}

/**
 * 更新书籍信息
 * @param {express.Request} req  请求
 * @param {express.Response} res  响应
 */
function update(req, res) {
  (async function () {
    //FIXME:req.body的数据安全性校验
    let bookshelf = {
      id: req.params.id,
      readStatus: req.body.readStatus,
      ranking: req.body.ranking,
      updatedTime: Date.now(),
    };

    let db = BookshelfDB.getInstance();
    await db.connect();
    let result = await db.update(bookshelf);
    //await db.close();
    util.log(`更新书架中的书籍相关信息：changes->${result}`);
    res.json(
      util.FormatJSONData(200, `更新书架中的书籍相关信息`, { changes: result })
    );
  })();
}

/**
 * 删除书架信息
 * @param {express.Request} req  请求
 * @param {express.Response} res  响应
 */
function remove(req, res) {
  (async function () {
    //FIXME:数据合法性校验
    let bookshelfId = req.params.id;
    let db = BookshelfDB.getInstance();
    await db.connect();
    let result = await db.remove(bookshelfId);
    //await db.close();
    util.log(`删除书架中的书籍信息：changes->${result}`);
    res.json(util.FormatJSONData(204, `删除书架中的书籍`, { changes: result }));
  })();
}

/**
 * 获取书籍总数
 * @param {express.Request} req  请求
 * @param {express.Response} res  响应
 */
function getCount(req, res) {
  (async function () {
    //FIXME:数据合法性校验
    let userId = req.session.user.id;
    let db = BookshelfDB.getInstance();
    await db.connect();
    let result = await db.getCount(userId);
    //await db.close();
    util.log(`书架中书籍总数：${result}`);
    res.json(util.FormatJSONData(200, `获取书架中书籍总数`, result));
  })();
}

/**
 * 更新书架中的书籍阅读状态
 * @param {express.Request} req  请求
 * @param {express.Response} res  响应
 */
function updateReadStatus(req, res) {
  (async function () {
    //FIXME:数据合法性校验
    let id = req.params.id;
    let readStatus = req.body.readStatus;

    let db = BookshelfDB.getInstance();
    await db.connect();
    let result = await db.updateReadStatus(id, readStatus);
    //await db.close();
    util.log(`更新书架中书籍阅读状态：${readStatus}`);
    res.json(
      util.FormatJSONData(200, `更新书架中书籍阅读状态`, { changes: result })
    );
  })();
}

/**
 * 更新书架中的书籍评分
 * @param {express.Request} req  请求
 * @param {express.Response} res  响应
 */
function updateRanking(req, res) {
  (async function () {
    //FIXME:数据合法性校验
    let id = req.params.id;
    let ranking = req.body.ranking;

    let db = BookshelfDB.getInstance();
    await db.connect();
    let result = await db.updateRanking(id, ranking);
    //await db.close();
    util.log(`更新书架中书籍评分：${ranking}`);
    res.json(
      util.FormatJSONData(200, `更新书架中书籍评分`, { changes: result })
    );
  })();
}

/**
 * 获取指定BookID的书架书籍信息
 * @param {express.Request} req  请求
 * @param {express.Response} res  响应
 */
function findByBookId(req, res) {
  (async function () {
    let db = BookshelfDB.getInstance();
    await db.connect();
    let result = await db.findByBookId(req.params.book_id, req.session.user.id);
    //await db.close();
    util.logFormat(`获取【${req.params.book_id}】书籍信息%O`, result);
    res.json(
      util.FormatJSONData(200, `获取指定书籍【${req.params.book_id}】`, result)
    );
  })();
}

/**
 * 获取指定书籍的综合评分
 * @param {express.Request} req  请求
 * @param {express.Response} res  响应
 */
function avgRankingByBookId(req, res) {
  (async function () {
    let db = BookshelfDB.getInstance();
    await db.connect();
    let result = await db.avgRankingByBookId(req.params.book_id);
    //await db.close();
    util.logFormat(`获取指定书籍【${req.params.book_id}】的综合评分%O`, result);
    res.json(
      util.FormatJSONData(
        200,
        `获取指定书籍【${req.params.book_id}】的综合评分`,
        result
      )
    );
  })();
}

module.exports = {
  find,
  findAll,
  add,
  update,
  remove,
  getCount,
  updateReadStatus,
  updateRanking,
  findByBookId,
  avgRankingByBookId,
};
