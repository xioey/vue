/**
 * 权限控制中间件
 * auth.js
 */

const express = require("express");
const util = require("../common/util");

/**
 * 是否登录
 * @param {express.Request} req 请求
 * @param {express.Response} res 响应
 * @param {express.NextFunction} next 下一个
 */
function isLogin(req, res, next) {
  if (!req.session.isLogin) {
    res.json(util.FormatJSONData(401, "无访问授权"));
  } else {
    next();
  }
}

/**
 * 角色是否为管理员
 * @param {express.Request} req 请求
 * @param {express.Response} res 响应
 * @param {express.NextFunction} next 下一个
 */
function isAdmin(req, res, next) {
  if (req.session.user.role !== 1) {
    res.json(util.FormatJSONData(403, "无访问权限"));
  } else {
    next();
  }
}

/**
 * 角色是否为管理员
 * @param {express.Request} req 请求
 * @param {express.Response} res 响应
 * @param {express.NextFunction} next 下一个
 */
function isSelfOfAdmin(req, res, next) {
  let id = Number.parseInt(req.params.id);
  if (req.session.user.id === id || req.session.user.role === 1) {
    next();
  } else {
    res.json(util.FormatJSONData(403, "无访问权限"));
  }
}

module.exports = {
  isLogin,
  isAdmin,
  isSelfOfAdmin,
};
