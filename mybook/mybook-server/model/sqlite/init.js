/**
 * SQLite3数据库初始化模块
 * model\sqlite\init.js
 * 1. npm install sqlite3
 * 2. API文档:https://github.com/TryGhost/node-sqlite3/wiki/API
 */

//导入Node.js文件模块
const fs = require("fs");
//导入SQLite3模块
const sqlite3 = require("sqlite3").verbose();
//导入工具模块
const util = require("../../common/util");
//导入配置模块
const config = require("../../common/config");

//SQL语句：创建Book数据表
const sqlCreateBook = `
 CREATE TABLE IF NOT EXISTS books(
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    title VARCHAR(255) NOT NULL,
    pic TEXT,
    local_pic TEXT,
    author VARCHAR(255),
    publisher VARCHAR(255),
    producer VARCHAR(255),
    subtitle VARCHAR(255),
    originalTitle VARCHAR(255),
    translator VARCHAR(255),
    pubdate VARCHAR(255),
    pages VARCHAR(255),
    price VARCHAR(255),
    binding VARCHAR(255),
    series VARCHAR(255),
    isbn VARCHAR(255) NOT NULL,
    intro TEXT,
    doubanId VARCHAR(255),
    created_time DATETIME NOT NULL,
    updated_time DATETIME NOT NULL
 );
`;

//SQL语句：创建User数据表
const sqlCreateUser = `
   CREATE TABLE IF NOT EXISTS users(
      id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
      username VARCHAR(255) NOT NULL UNIQUE,
      password VARCHAR(255) NOT NULL,
      nickname VARCHAR(255),
      truename VARCHAR(255),
      avatar VARCHAR(255),
      role INTEGER NOT NULL,
      last_login_time DATETIME NOT NULL,
      last_login_ip VARCHAR(255) NOT NULL,
      login_count INTEGER DEFAULT 0,
      created_time DATETIME NOT NULL,
      created_ip VARCHAR(255) NOT NULL,
      updated_time DATETIME NOT NULL
  );
`;

//SQL语句：创建User数据表中的第一个用户
const sqlCreateAdminUser = `
  INSERT INTO users(
    username, password, nickname, truename, avatar,
    role, last_login_time, last_login_ip, login_count, created_time,
    created_ip, updated_time
  ) VALUES (
    'admin', '${util.md5pwd(
      "123456"
    )}', '系统管理员', '超级管理员', './images/default.png', 
    1, 0, 'never login', 0, ${Date.now()}, 'system init', ${Date.now()}
   );
  `;

//SQL语句：创建User数据表
const sqlCreateBookshelf = `
  CREATE TABLE IF NOT EXISTS bookshelves(
  id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  user_id INTEGER NOT NULL,
  book_id INTEGER NOT NULL,
  read_status INTEGER DEFAULT 0,
  ranking INTEGER DEFAULT 0,
  created_time DATETIME NOT NULL,
  updated_time DATETIME NOT NULL
);`;

//SQL语句：创建User数据表
const sqlCreateReview = `
CREATE TABLE IF NOT EXISTS reviews(
  id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  user_id INTEGER NOT NULL,
  book_id INTEGER NOT NULL,
  title VARCHAR(255) NOT NULL,
  content TEXT,
  useful INTEGER DEFAULT 0,
  useless INTEGER DEFAULT 0,
  created_time DATETIME NOT NULL,
  updated_time DATETIME NOT NULL
);`;

/**
 * SqliteDB构造函数
 * @constructor
 */
function SqliteDB() {
  this.dbFile = config.dbFile;
  //如果数据库nylibrary.db文件不存在则创建数据库文件
  if (!fs.existsSync(this.dbFile)) {
    util.log("创建SQLite数据库文件【mylibrary.db】");
    this.initDB();
  }
}

/**
 * 执行初始化数据库的SQL语句
 */
SqliteDB.prototype.initDB = function () {
  let db = new sqlite3.Database(this.dbFile, (err) => {
    if (err) {
      util.err(err);
    }
  });
  db.serialize(() => {
    db.run(sqlCreateBook);
    util.log("数据表Book创建成功");
    db.run(sqlCreateUser);
    util.log("数据表User创建成功");
    db.run(sqlCreateAdminUser);
    util.log("管理员用户创建成功");
    db.run(sqlCreateBookshelf);
    util.log("数据表Bookshelf创建成功");
    db.run(sqlCreateReview);
    util.log("数据表Review创建成功");
  });
  //SQLite数据库是基于文件，大量的资源消耗在文件的打开与关闭上会降低效率
  //   db.close((err) => {
  //     if (err) {
  //       util.err(err);
  //     }
  //  });
};

module.exports = SqliteDB;
