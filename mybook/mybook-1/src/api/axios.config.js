/**
 * Axios 公共配置模块
 */

import axios from "axios";
import router from "../router/index";
import myMessageBox from "../components/my-message-box";

// const baseURL = "http://127.0.0.1:3004";
const baseURL = "/api";

//axios全局设置
axios.defaults.timeout = 10000; //设置访问超时时间10秒
axios.defaults.baseURL = baseURL; //设置后端基础URL

//请求拦截器
axios.interceptors.request.use(
  function (config) {
    //请求发送前进行设置
    return config;
  },
  function (error) {
    console.error("===请求拦截器：错误处理===");
    myMessageBox("请求服务器错误！");
    return Promise.reject(error);
  }
);

//响应拦截器
axios.interceptors.response.use(
  function (response) {
    //处理响应的数据
    if (response.status && response.status === 200) {
      let data = response.data;
      if (data.code === 500) {
        myMessageBox(data.msg);
        router.replace("/");
      }
    }
    return response;
  },
  function (error) {
    console.error("===响应拦截器：错误处理===");
    myMessageBox("服务器响应错误！");
    return Promise.reject(error);
  }
);

export default axios;
