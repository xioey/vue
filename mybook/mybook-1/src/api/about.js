/**
 * 访问"关于"接口网络模块
 */

import axios from "./axios.config";
import util from "./util";

class AboutApi {
  async getSiteInfo() {
    let result = await axios.get("/");
    return util.jsonTransfer(result.data);
  }
}

export default AboutApi;
