/**
 * 访问"用户"接口网络模块
 */
import axios from "./axios.config";
import util from "./util";

class UserApi {
  async login(data) {
    let result = await axios.post("/users/login", data);
    return util.jsonTransfer(result.data);
  }
  async logout() {
    let result = await axios.get("/users/logout");
    return util.jsonTransfer(result.data);
  }

  async find(userId) {
    let result = await axios.get(`/users/${userId}`);
    return util.jsonTransfer(result.data);
  }

  async changeProfile(user) {
    let result = await axios.patch(`/users/change-info`, user);
    return util.jsonTransfer(result.data);
  }

  async checkPassword(data) {
    let result = await axios.post(`/users/check-password`, data);
    return util.jsonTransfer(result.data);
  }

  async changePassword(data) {
    let result = await axios.patch(`/users/change-password`, data);
    return util.jsonTransfer(result.data);
  }

  async findAll(limit, offset) {
    let result = await axios.get(`/users?limit=${limit}&offset=${offset}`);
    return util.jsonTransfer(result.data);
  }

  async getCount() {
    let result = await axios.get(`/users/count`);
    return util.jsonTransfer(result.data);
  }

  async add(user) {
    let result = await axios.post(`/users`, user);
    return util.jsonTransfer(result.data);
  }

  async edit(user) {
    let result = await axios.put(`/users/${user.id}`, user);
    return util.jsonTransfer(result.data);
  }

  async delete(userId) {
    let result = await axios.delete(`/users/${userId}`);
    return util.jsonTransfer(result.data);
  }

  async resetPassword(userId) {
    let result = await axios.patch(`/users/reset-password/${userId}`);
    return util.jsonTransfer(result.data);
  }
}

export default UserApi;
