/**
 * 访问"评论"接口网络模块
 */

import axios from "./axios.config";
import util from "./util";

class ReviewApi {
  async findAllByBookId(bookId, limit, offset, orderBy, sort) {
    let result = await axios.get(
      `/reviews?book_id=${bookId}&limit=${limit}&offset=${offset}&order_by=${orderBy}&sort=${sort}`
    );
    return util.jsonTransfer(result.data);
  }

  async getCountByBookId(bookId) {
    let result = await axios.get(`/reviews/count?book_id=${bookId}`);
    return util.jsonTransfer(result.data);
  }

  async add(review) {
    let result = await axios.post("/reviews", review);
    return util.jsonTransfer(result.data);
  }

   async edit(review) {
    let result = await axios.put(`/reviews/${review.id}`, review);
    return util.jsonTransfer(result.data);
  }

  async delete(id) {
    let result = await axios.delete(`/reviews/${id}`);
    return util.jsonTransfer(result.data);
  }

  async updateReviewUseful(reviewId){
    let result = await axios.patch(`/reviews/useful/${reviewId}`);
    return util.jsonTransfer(result.data);
  }

  async updateReviewUseless(reviewId){
    let result = await axios.patch(`/reviews/useless/${reviewId}`);
    return util.jsonTransfer(result.data);
  }

  async findAllByUserId(userId, limit, offset, orderBy, sort) {
    let result = await axios.get(
      `/reviews/user/?user_id=${userId}&limit=${limit}&offset=${offset}&order_by=${orderBy}&sort=${sort}`
    );
    return util.jsonTransfer(result.data);
  }

  async getCountByUserId(userId) {
    let result = await axios.get(`/reviews/user/count?user_id=${userId}`);
    return util.jsonTransfer(result.data);
  }
}

export default ReviewApi;
