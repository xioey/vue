/**
 * 工具模块
 */

function jsonTransfer(obj) {
  //不是JSONObject的时候，终止递归
  if (typeof obj !== "object" || obj === null) {
    return obj;
  }
  //JSONArray进行迭代

  if (Array.isArray(obj)) {
    return obj.map(jsonTransfer);
  }

  //JSONObject递归访问
  return Object.keys(obj).reduce((result, key) => {
    const camelCaseKey = key.replace(/_([a-z])/g, (_, letter) => letter.toUpperCase());
    result[camelCaseKey] = jsonTransfer(obj[key]);
    return result;
  }, {});
}

export default {
  jsonTransfer
};
