/**
 * 访问"书架"接口网络模块
 */
import axios from "./axios.config";
import util from "./util";

class BookShelfApi {
  async findAll(limit, offset, orderBy, sort) {
    let result = await axios.get(
      `/bookshelves?limit=${limit}&offset=${offset}&order_by=${orderBy}&sort=${sort}`
    );
    return util.jsonTransfer(result.data);
  }

  async getCount() {
    let result = await axios.get(`/bookshelves/count`);
    return util.jsonTransfer(result.data);
  }

  async add(bookId) {
    let result = await axios.post(`/bookshelves`, {bookId: bookId});
    return util.jsonTransfer(result.data);
  }

  // async edit(book) {
  //   let result = await axios.put(`/books/${book.id}`, book);
  //   return util.jsonTransfer(result.data);
  // }

  async delete(id) {
    let result = await axios.delete(`/bookshelves/${id}`);
    return util.jsonTransfer(result.data);
  }

  // async search(keyword) {
  //   let result = await axios.get(`/books/search?q=${keyword}`);
  //   return util.jsonTransfer(result.data);-
  // }

  async findByBookId(bookId) {
    let result = await axios.get(`/bookshelves/book/${bookId}`);
    return util.jsonTransfer(result.data);
  }

  async updateReadStatus(id, readStatus){
    let result = await axios.patch(`/bookshelves/read-status/${id}`, {readStatus: readStatus});
    return util.jsonTransfer(result.data);
  }

  async updateRanking(id,ranking){
    let result = await axios.patch(`/bookshelves/ranking/${id}`, {ranking: ranking});
    return util.jsonTransfer(result.data);
  }

  async avgRankingByBookId(bookId){
    let result = await axios.get(`/bookshelves/average-ranking/${bookId}`);
    return util.jsonTransfer(result.data);
  }
}

export default BookShelfApi;
