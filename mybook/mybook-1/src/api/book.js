/**
 * 访问"书籍"接口网络模块
 */
import axios from "./axios.config";
import util from "./util";

class BookApi {
  async findAll(limit, offset, orderBy, sort) {
    let result = await axios.get(
      `/books?limit=${limit}&offset=${offset}&order_by=${orderBy}&sort=${sort}`
    );
    return util.jsonTransfer(result.data);
  }

  async getCount() {
    let result = await axios.get(`/books/count`);
    return util.jsonTransfer(result.data);
  }

  async find(bookId) {
    let result = await axios.get(`/books/${bookId}`);
    return util.jsonTransfer(result.data);
  }

  async fetchDouban(isbn) {
    let result = await axios.get(`/books/fetch/douban/${isbn}`);
    return util.jsonTransfer(result.data);
  }

  async add(book) {
    let result = await axios.post(`/books`, book);
    return util.jsonTransfer(result.data);
  }

  async edit(book) {
    let result = await axios.put(`/books/${book.id}`, book);
    return util.jsonTransfer(result.data);
  }

  async delete(bookId) {
    let result = await axios.delete(`/books/${bookId}`);
    return util.jsonTransfer(result.data);
  }

  async search(keyword) {
    let result = await axios.get(`/books/search?q=${keyword}`);
    return util.jsonTransfer(result.data);
  }

  async findByIsn(isbn) {
    let result = await axios.get(`/books/isbn/${isbn}`);
    return util.jsonTransfer(result.data);
  }
}

export default BookApi;
