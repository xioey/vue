import "./assets/main.css";
import "./assets/normalize.css";

import { createApp } from "vue";
import pinia from "./stores";

import App from "./App.vue";
import router from "./router";

//import myMessageBox from "./components/my-message-box";
//import myBreadcrumb from "./components/my-breadcrumb";
import myComponont from "./components/my-components";
import myFunction from "./components/my-function";

const app = createApp(App);

app.use(pinia);
app.use(router);

//全局属性绑定消息框功能方法
//app.config.globalProperties.$myMessageBox = myMessageBox;
//全局属性绑定返回方法
// app.config.globalProperties.$goBack = function () {
//   this.$router.go(-1);
// };

//app.use(myBreadcrumb);
app.use(myComponont);
app.use(myFunction);

app.mount("#app");
