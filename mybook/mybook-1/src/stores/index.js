import { createPinia } from "pinia";
import piniaPluginPersistedstate from "pinia-plugin-persistedstate";

const pinia = createPinia();
//加载状态持久化存储：localStorage
pinia.use(piniaPluginPersistedstate);

export default pinia;
