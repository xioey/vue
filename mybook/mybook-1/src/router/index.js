import { createRouter, createWebHistory } from "vue-router";
import { useUserStore } from "../stores/user";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      redirect: "/book"
    },
    {
      path: "/book",
      name: "book-index",
      component: () => import("../views/book/IndexView.vue"),
      meta: {
        requiresAuth: true,
        title: "书库"
      },
      children: [
        {
          path: "",
          name: "book-list",
          component: () => import("../views/book/BookListView.vue"),
          meta: {
            requiresAuth: true,
            title: "列表"
          }
        },
        {
          path: ":id",
          name: "book-detail",
          component: () => import("../views/book/BookDetailView.vue"),
          meta: {
            requiresAuth: true,
            title: "详情"
          }
        },
        {
          path: "add",
          name: "book-add",
          component: () => import("../views/book/BookAddView.vue"),
          meta: {
            requiresAuth: true,
            title: "新增"
          }
        },
        {
          path: "edit/:id",
          name: "book-edit",
          component: () => import("../views/book/BookEditView.vue"),
          meta: {
            requiresAuth: true,
            title: "编辑"
          }
        }
      ]
    },
    {
      path: "/bookshelf",
      name: "bookshelf-index",
      component: () => import("../views/bookshelf/IndexView.vue"),
      meta: {
        requiresAuth: true,
        title: "我的书架"
      },
      children: [
        {
          path: "",
          name: "bookshelf-list",
          component: () => import("../views/bookshelf/bookshelfListView.vue"),
          meta: {
            requiresAuth: true,
            title: "列表"
          }
        }
      ]
    },
    {
      path: "/review",
      name: "review-index",
      component: () => import("../views/review/IndexView.vue"),
      meta: {
        requiresAuth: true,
        title: "我的评论"
      },
      children: [
        {
          path: "",
          name: "review-list",
          component: () => import("../views/review/ReviewListView.vue"),
          meta: {
            requiresAuth: true,
            title: "列表"
          }
        }
      ]
    },
    {
      path: "/user",
      name: "user-index",
      component: () => import("../views/user/IndexView.vue"),
      meta: {
        requiresAuth: true,
        title: "我的"
      },
      children: [
        {
          path: "",
          name: "user-profile",
          alias: "profile",
          component: () => import("../views/user/ProfileView.vue"),
          meta: {
            requiresAuth: true,
            title: "个人信息"
          }
        },
        {
          path: "change-profile/:id",
          name: "user-profile-esit",
          component: () => import("../views/user/ProfileEditView.vue"),
          meta: {
            requiresAuth: true,
            title: "修改个人信息"
          }
        },
        {
          path: "change-password/:id",
          name: "user-password-esit",
          component: () => import("../views/user/PasswordEditView.vue"),
          meta: {
            requiresAuth: true,
            title: "修改密码"
          }
        }
      ]
    },

    {
      path: "/login",
      name: "user-login",
      component: () => import("../views/user/LoginView.vue"),
      meta: {
        requiresAuth: false,
        title: "登录"
      }
    },
    {
      path: "/admin",
      name: "admin-index",
      component: () => import("../views/admin/IndexView.vue"),
      meta: {
        requiresAuth: true,
        requiresAdmin: true,
        title: "系统管理"
      },
      children: [
        {
          path: "",
          name: "user-list",
          alias: "user",
          component: () => import("../views/admin/UserListView.vue"),
          meta: {
            requiresAuth: true,
            requiresAdmin: true,
            title: "用户管理"
          }
        }
      ]
    },
    {
      path: "/about",
      name: "about",
      component: () => import("../views/AboutView.vue"),
      meta: {
        requiresAuth: false,
        title: "关于"
      }
    },
    {
      path: "/:pathMatch(.*)*",
      name: "not-found",
      component: () => import("../views/NotFoundView.vue"),
      meta: {
        requiresAuth: false,
        title: "404"
      }
    }
  ]
});

//前置路由守卫
router.beforeEach((to) => {
  const store = useUserStore();
  const isLogin = store.isLogin;
  const isAdmin = store.isAdmin;
  console.log(`to:${to.fullPath},isLogin:${isLogin}`);

  //登录状态服务端校验
  if (to.meta.requiresAuth && isLogin) {
    console.log("后端校验权限");
    store.isAuth();
  }

  //防止登录页循环访问
  if (isLogin && to.name === "user-login") {
    return { path: "/" };
  }

  //未登录且需要访问权限，跳转到登录路由
  if (to.meta.requiresAuth && !isLogin) {
    return {
      path: "/login"
    };
  }

  //需要管理员权限，跳转到用户个人信息路由
  if (to.meta.requiresAdmin && !isAdmin) {
    return {
      path: "/user"
    };
  }
});

export default router;
