import MyMessageBox from "./MyMessageBox.vue";
import MyBreadcrumb from "./MyBreadcrumb.vue";
import MyPagination from "./MyPagination.vue";
import MyModel from "./MyModel.vue";

export default {
  install: (app) => {
    app.component("MyBreadcrumb", MyBreadcrumb);
    app.component("MyPagination", MyPagination);
    app.component("MyModel", MyModel);
    app.component("MyMessageBox", MyMessageBox);
  }
};
