import { createApp } from "vue";
import MyMessageBox from "./MyMessageBox.vue";

export default {
  install: (app) => {
    app.config.globalProperties.$myMessageBox = function (msg) {
      const dom = document.createElement("div"); //创建消息框容器
      document.body.appendChild(dom); //容器放入当前的页面
      const app = createApp(MyMessageBox); //创建Vue实例
      const instance = app.mount(dom);
      instance.show(msg);
      //3秒后关闭
      setTimeout(() => {
        dom.remove();
        app.unmount();
      }, 3000);
    };
    app.config.globalProperties.$goBack = function () {
      this.$router.go(-1);
    };
  }
};
