import { createRouter, createWebHistory } from "vue-router";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "admin-index",
      component: () => import("../views/IndexView.vue"),
      children: [
        {
          path: "",
          name: "dashboard",
          alias: "/dashboard",
          component: () => import("../views/DashboardView.vue")
        },
        {
          path: "manager/classes",
          name: "manager-classes",
          component: () => import("../views/ClassesView.vue")
        },
        {
          path: "manager/student",
          name: "manager-student",
          component: () => import("../views/StudentView.vue")
        },
        {
          path: "manager/course",
          name: "manager-course",
          component: () => import("../views/CourseView.vue")
        },
        {
          path: "manager/score",
          name: "manager-score",
          component: () => import("../views/ScoreView.vue")
        },
        {
          path: "manager/score-query",
          name: "manager-score-query",
          component: () => import("../views/ScoreQueryView.vue")
        }
      ]
    },
    {
      path: "/login",
      name: "user-login",
      component: () => import("../views/Loginview.vue")
    }
  ]
});

export default router;
