import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    isIndex:true,
    userInfo:[] //放来给个人中心用的
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  }
})
