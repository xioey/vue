import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import ElementUI from "element-ui";
import "./assets/css/global.css";
import "element-ui/lib/theme-chalk/index.css";
import axios from "axios";
import echarts from "echarts";
axios.defaults.baseURL = "http://127.0.0.1:3004/api/";

Vue.config.productionTip = false;
Vue.prototype.$http = axios;
Vue.prototype.$echarts = echarts;
Vue.use(ElementUI);

const app = new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
