import Vue from "vue";
import VueRouter from "vue-router";
import login from "@/views/login/login";
import index from "@/views/index/index";
import welcome from "@/views/welcome/welcome";
import stafflist from "@/views/staff/stafflist";
// import statistics from "@views/statistics/goodsSell";
const goods = () =>
  import(/* webpackChunkName: "goods" */ "@/views/goods/goodslist");
const goodsSell = () => import("@/views/statistics/goodsSell");
const goodsstastic = () => import("@/views/statistics/goodsstastic");
const personal = () => import("@/views/personal/personal");

Vue.use(VueRouter);

// 防止路由重复点击时报相同路由的错
const originalPush = VueRouter.prototype.push;
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch((err) => err);
};

const routes = [
  { path: "/", redirect: "/login" },
  { path: "/login", name: "login", component: login },
  {
    path: "/index",
    meta: {
      requireAuth: true, //此路由需要认证
    },
    component: index,
    children: [
      { path: "/", component: welcome },
      { path: "/staff", component: stafflist },
      { path: "/goods", component: goods },
      { path: "/salescharts", component: goodsSell },
      { path: "/worktimecharts", component: goodsstastic },
      { path: "/personal", component: personal },
    ],
  },
];

const router = new VueRouter({
  routes,
});

router.beforeEach((to, from, next) => {
  if (to.meta.requireAuth) {
    // 判断该路由是否需要登录权限
    if (window.sessionStorage.getItem("token")) {
      // 通过vuex state获取当前的token是否存在
      next();
    } else {
      app.$message.info("请先登录~");
      next({
        path: "/login",
      });
    }
  } else {
    next();
  }
});

export default router;
