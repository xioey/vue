const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const app = express();

let user = require("./routes/user");
let staff = require("./routes/staff");
let goods = require("./routes/goods");
const post = 3004;

app.listen(post, () => {
  console.log("服务器监听于" + post + "端口。");
});
// json格式
app.use(express.json());
// 跨域
app.use(cors());
// url码
app.use(express.urlencoded({ extended: false }));
// 设置最大
app.use(bodyParser.json({ limit: "10mb" }));
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));

// 使用路由 路径为localhost:3002/api/
app.use("/api/user/", user);
app.use("/api/staff/", staff);
app.use("/api/goods/", goods);

module.exports = app;
