const express = require("express");
const router = express.Router();
const db = require("../database");

router.get("/goodslist", (req, res) => {
  let currentPage = req.query.currentPage; //当前页
  let pageSize = parseInt(req.query.pageSize); // 页大小
  let start = (currentPage - 1) * pageSize;
  let sql = db.goodslistByPage(start, pageSize);
  console.log("商品列表", currentPage, pageSize);
  console.log(sql);
  db.query(sql).then(
    (data) => {
      db.query(db.goodslistTotalNum()).then((data2) => {
        const total = data2[0].total;
        res.send({
          code: "200",
          message: "查询成功！",
          data: data,
          total: total,
        });
      });
    },
    (err) => {
      res.send({ code: "401", message: "查询失败" });
    }
  );
});

router.get("/allgoodslist", (req, res) => {
  let sql = db.goodslist();
  console.log(sql);
  db.query(sql).then(
    (data) => {
      res.send({ code: "200", message: "查询成功！", data: data });
    },
    (err) => {
      res.send({ code: "401", message: "查询失败" });
    }
  );
});

router.get("/categorylist", (req, res) => {
  let sql = db.queryCategoryList();
  db.query(sql).then(
    (data) => {
      res.send({ code: "200", message: "查询成功！", data: data });
    },
    (err) => {
      res.send({ code: "401", message: "查询失败" });
    }
  );
});

router.post("/findGoodsById", (req, res) => {
  let id = parseInt(req.body.id);
  let sql = db.findGoodsById(id);
  console.log(sql);
  db.query(sql).then(
    (data) => {
      res.send({ code: "200", message: "查找成功！", data: data });
    },
    (err) => {
      res.send({ code: "401", message: "查找失败" });
    }
  );
});

router.post("/updateGoodsNumById", (req, res) => {
  let updateNumForm = req.body.updateNumForm;
  let sql = db.updateGoodsNumById(updateNumForm);
  console.log(sql);
  db.query(sql).then(
    (data) => {
      let sql2 = db.addGoodsMsgInfo(updateNumForm);
      console.log(sql2);
      db.query(sql2).then((data2) => {
        res.send({ code: "200", message: "添加成功！" });
      });
    },
    (err) => {
      res.send({ code: "401", message: "修改失败" });
    }
  );
});

router.post("/addGoods", (req, res) => {
  let goodsForm = req.body.goodsForm;
  let sql = db.addGoods(goodsForm);
  console.log(sql);
  db.query(sql).then(
    (data) => {
      res.send({ code: "200", message: "添加成功！" });
    },
    (err) => {
      res.send({ code: "401", message: "添加失败" });
    }
  );
});

router.post("/updateGoods", (req, res) => {
  let editGoodsForm = req.body.editGoodsForm;
  let sql = db.updateGoods(editGoodsForm);
  console.log(sql);
  db.query(sql).then(
    (data) => {
      res.send({ code: "200", message: "修改成功！" });
    },
    (err) => {
      res.send({ code: "401", message: "修改失败" });
    }
  );
});

router.post("/goodsInfoList", (req, res) => {
  let id = req.body.id;
  let sql = db.goodsInfoListById(id);
  db.query(sql).then(
    (data) => {
      res.send({ code: "200", message: "查询成功！", data: data });
    },
    (err) => {
      res.send({ code: "401", message: "查询失败" });
    }
  );
});

router.post("/queryGoodsInfoByMonth", (req, res) => {
  let MonthForm = req.body.MonthForm;
  let id = req.body.id;
  let sql = db.queryGoodsInfoByMonthAndId(MonthForm, id);
  let sql2 = db.queryGoodsInfoByMonthAndIdCountResultNumber(MonthForm, id);
  console.log(sql);
  console.log(sql2);
  db.query(sql).then(
    (data) => {
      db.query(sql2).then((data2) => {
        res.send({
          code: "200",
          message: "查询成功！",
          data: data,
          result: data2[0].result,
        });
      });
    },
    (err) => {
      res.send({ code: "401", message: "查询失败" });
    }
  );
});

router.post("/deleteGoodsById", (req, res) => {
  let id = parseInt(req.body.id);
  let sql = db.deleteGoodsById(id);
  console.log(sql);
  db.query(sql).then(
    (data) => {
      res.send({ code: "200", message: "删除成功！" });
    },
    (err) => {
      res.send({ code: "200", message: "删除失败！" });
    }
  );
});
router.post("/findGoodsByName", (req, res) => {
  let name = req.body.name;
  let sql = db.findGoodsByName(name);
  db.query(sql).then(
    (data) => {
      res.send({ code: "200", message: "查找成功！", data: data });
    },
    (err) => {
      res.send({ code: "401", message: "查找失败" });
    }
  );
});
router.post("/querySellGoodsInfoByMonth", (req, res) => {
  let MonthForm = req.body.MonthForm;
  // console.log(MonthForm,MonthForm === [])  判断真空直接判断length
  let id = req.body.id ? req.body.id : 1;

  let sql =
    MonthForm.length === 0
      ? db.querySellGoodsInfoById(id)
      : db.querySellGoodsInfoByMonthAndId(MonthForm, id);
  console.log(sql);
  db.query(sql).then(
    (data) => {
      res.send({ code: "200", message: "查询成功！", data: data });
    },
    (err) => {
      res.send({ code: "401", message: "查询失败" });
    }
  );
});

router.get("/findGoodsInfolist", (req, res) => {
  let currentPage = req.query.currentPage; //当前页
  let pageSize = parseInt(req.query.pageSize); // 页大小
  let start = (currentPage - 1) * pageSize;
  let sql = db.GoodslistByPage(start, pageSize);
  console.log("商品列表", currentPage, pageSize);
  console.log(sql);
  db.query(sql).then(
    (data) => {
      db.query(db.GoodslistTotalNum()).then((data2) => {
        const total = data2[0].total;
        res.send({
          code: "200",
          message: "查询成功！",
          data: data,
          total: total,
        });
      });
    },
    (err) => {
      res.send({ code: "401", message: "查询失败" });
    }
  );
});

// 按商品名查询
router.post("/goodsInfoListByName", (req, res) => {
  let name = req.body.name;
  console.log(name);
  let sql = db.goodsInfoListByName(name);
  db.query(sql).then(
    (data) => {
      res.send({ code: "200", message: "查找成功！", data: data });
    },
    (err) => {
      res.send({ code: "401", message: "查找失败" });
      console.log(err);
    }
  );
});
module.exports = router;
