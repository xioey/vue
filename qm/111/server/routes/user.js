const express = require("express");
const router = express.Router();
const db = require("../database");

//登录路由
router.post("/login", (req, res) => {
  let username = req.body.username;
  let password = req.body.password;
  console.log(username, password);
  let sql = db.login(username, password);
  console.log(sql);
  db.query(sql).then(
    (data) => {
      if (data.length) {
        console.log("用户登录成功！");
        res.send({
          code: "200",
          message: "登录成功！",
          data: data,
        });
      } else {
        res.send({ code: "401", message: "账号或者密码错误，登录失败！" });
      }
    },
    (err) => {
      res.send({ code: "401", message: "账号或者密码错误，登录失败！" });
    }
  );
});

// 注册
router.post("/register", (req, res) => {
  let username = req.body.username;
  let password = req.body.password;
  console.log(username, password);
  let sql = db.register(username, password);
  db.query(sql).then(
    (data) => {
      console.log(data);
      res.send({ code: "200", message: "注册成功！" });
    },
    (err) => {
      res.send({
        code: "401",
        message: "服务器不小心走神了！稍后再试试吧！",
      });
    }
  );
});

module.exports = router;
